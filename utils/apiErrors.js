/**
 * List of errors
 *
 * @author DroidBoyJr
 * created on 2016-08-23
 */

var placeHolderFormatter = require('strformat');

/*
 * Defines the error object to be thrown or returned in case of any error condition.
 * It stores the error code that encapsulates error number, severity of error, facility that created an error.
 * Additional parameters can be put in this class, which could be used in cases where the additional
 * information is required to describe an error.
 *
 *
 *  Values are 32 bit values laid out as follows:
 *
 *  +---+-+-+-----------------------+-------------------------------+
 *  |Sev|R|R|     Facility          |               Code            |
 *  +---+-+-+-----------------------+-------------------------------+
 *
 *  where
 *
 *      Sev - is the severity code (2 bits)
 *
 *          00 - Success
 *          01 - Informational
 *          10 - Warning
 *          11 - Error
 *
 *      R - is a reserved bit.
 *
 *      R - is a reserved bit
 *
 *      Facility - is the facility code (12 bits)
 *
 *      Code - is the facility's error number (16 bits)
 *
 */

/*=============================================>>>>>
= Base error codes =
===============================================>>>>>*/

var SEVERITY_ERROR = 0x00000003;
var SEVERITY_WARNING = 0x00000002;
var SEVERITY_INFORMATIONAL = 0x00000001;

var SUCCESS = 0;

var FACILITIES = {
    APPLICATION: 0x100,
    USER: 0x101,
    NODE_SKU: 0x102,
    CONSUMER_SUPPLIERNODE: 0x103,

};

/*= End of Base error codes =*/
/*=============================================<<<<<*/

/*=============================================>>>>>
= Application errors =
===============================================>>>>>*/

var applicationErrors = {
    INVALID_API_KEY: {
        status_code: 0xC1000001,
        message: "Invalid API key"
    },
    INVALID_SESSION: {
        status_code: 0xC1000002,
        message: "Invalid session"
    },
    INTERNAL_ERROR: {
        status_code: 0xC1000003,
        message: "Server error"
    },
    UNAUTHORIZED_REQUEST: {
        status_code: 0xC1000004,
        message: "Unauthorized request"
    },
    INVALID_PARAMETERS: {
        status_code: 0xC1000005,
        message: "Invalid parameters"
    },
    INVALID_PAYLOAD_CHECKSUM: {
        status_code: 0xC1000006,
        message: "Incorrect payload checksum"
    },
    REQUEST_TIMEOUT: {
        status_code: 0xC1000007,
        message: "Request timeout"
    }
    
};

/*= End of Application errors =*/
/*=============================================<<<<<*/

/*=============================================>>>>>
= User errors =
===============================================>>>>>*/

var userErrors = {
    REGISTRATION_FAILED: {
        status_code: 0xC1010001,
        message: "Registration failed"
    },
    LOGIN_FAILED: {
        status_code: 0xC1010002,
        message: "Username or password incorrect"
    },
    NO_SUCH_USER: {
        status_code: 0xC1010003,
        message: "No such user exist"
    },
    EMAIL_EXISTS: {
        status_code: 0xC1010004,
        message: "Email already exists"
    },
    NO_PARAMETERS_RECEIVED: {
        status_code: 0xC1010005,
        message: "No parameters found"
    },
    NO_NAME_RECEIVED: {
        status_code: 0xC1010006,
        message: "No name field found"
    },
    NO_PASSWORD_RECEIVED: {
        status_code: 0xC1010007,
        message: "No password field found"
    },
    NO_EMAIL_RECEIVED: {
        status_code: 0xC1010008,
        message: "No email field found"
    },
    INVALID_EMAIL_RECEIVED: {
        status_code: 0xC1010009,
        message: "No email field found"
    }
};

 var nodeSkuErrors ={
    DUPLICATE_SKUNODE_PAIR : {
        status_code :  0xC1000008,
        message : "Duplicate Node SKU Pair"
    }

 };
 
 
const firmErrors = {
    UNDEFINED: {
        status_code: 0xC1010009,
        message: "Found Undefined"
    }
};
var consumerSupplierNodeError = {
     CONSUMERSUPPLIERNODECHECK :{
        status_code :  0xC1000008,
        message : "Duplicate consumer supplier node "
     }
}

/*= End of User errors =*/
/*=============================================<<<<<*/


// Restaurant Manager 

const restaurantErrors = {
    EMAIL_EXISTS: {
        status_code: 1,
        message: "Email already exists"
    }
};
/*=============================================>>>>>
= Private methods =
===============================================>>>>>*/

/**
 * Get error object with formatted message with place holders replaced with
 * arguments
 * @return {Object} error object
 */
var getFormattedError = function() {
    var paramsArray = Array.prototype.splice.call(arguments, 0);
    var error = paramsArray[0];
    var placeHolderParams = paramsArray.slice(1, paramsArray.length);

    var errorMessage = error.message;
    error.message = placeHolderFormatter(errorMessage, placeHolderParams);

    return error;
}

/*= End of Private methods =*/
/*=============================================<<<<<*/

module.exports = {
    SUCCESS: SUCCESS,
    APPLICATION: applicationErrors,
    NODESKU:nodeSkuErrors,
    CONSUMERSUPPLIERNODE :consumerSupplierNodeError,
    USER: userErrors,
    FIRM: firmErrors,
    RESTAURANT: restaurantErrors,
    getFormattedError: getFormattedError,
    
}