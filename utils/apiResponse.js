/**
 * The apiResponse is a helper utility for sending response to user
 *
 * @author DroidBoyJr
 * created on 2016-08-23
 */

var mongoose = require('mongoose'),
	async = require('async');
var apiErrors = require("./apiErrors.js");
var config = require("../config/config.js");

/*=============================================>>>>>
= Public methods =
===============================================>>>>>*/

/**
 * Send simple response with mentioned response status code
 * @param  {JSON Object/Array} data       - Data to be returned
 * @param  {int} status                   - Http status code to be returned
 * @param  {HttpResponse} res             - NodeJS response object
 */
var sendResponse = function(data, status, res){
  var response = {
      	  				status: {
        						message: "success",
        						status_code: 0
        					},
                  payload: data
                };

  res.status(status).json(response);
};

/**
 * Send response of requested paginated data
 * @param  {Array} data       - Data to be returned
 * @param  {int} pageNumber   - Current page number
 * @param  {int} start        - Starting record index number
 * @param  {int} total        - Total records for query considering all pages
 * @param  {int} status       - Http status code to be returneds
 * @param  {HttpResponse} res - NodeJS response object
 */
var sendPaginationResponse = function(data, pageNumber, start, total, status, res){
  var response = {  success: true,
                    result: data,
                    meta: {
                      page_number: pageNumber,
                      start: start,
                      total: total
                    }};

  res.status(status).json(response);
};

/**
 * Return error to user
 * @param  {Object} error     - ApiError object
 * @param  {int} status       - Http status code to be returneds
 * @param  {HttpResponse} res - NodeJS response object
 */
var sendError = function(error, data, status, res){
	var response = {  success: false,
	                status: error,
	                payload: data
	              };

	res.status(status).json(response);
};

/**
 * Return error to user
 * @param  {Object} error     - ApiError object
 * @param  {int} status       - Http status code to be returneds
 * @param  {HttpResponse} res - NodeJS response object
 */
var getStandardDBResponseHandler = function(res, withResult) {
	return function(err, result) {
    if (err) {
      return sendError(apiErrors.APPLICATION.INTERNAL_ERROR, null, 500, res);
    } else {
      return sendResponse(withResult ? result : null, 200, res);
    }
  };
};



/*= End of Public methods =*/
/*=============================================<<<<<*/

module.exports = {
    sendResponse: sendResponse,
    getStandardDBResponseHandler: getStandardDBResponseHandler,
    sendPaginationResponse: sendPaginationResponse,
    sendError: sendError
    
};
