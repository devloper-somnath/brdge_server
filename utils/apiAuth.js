/**
 * Middleware for header validation, session validation checks and API keys check
 *
 * @author DroidBoyJr
 * created on 2016-08-23
 */

var mongoose = require('mongoose');
var User = require('../models/user');
var apiResponse = require('../utils/apiResponse.js');
var apiErrors = require('../utils/apiErrors.js');
var jwt = require('jwt-simple');
var config = require('../config/config.js');


/**
 * Authorize admin API key and session token
 */
var adminAuthorization = function(req, res, next) {
    // Check session token header exists
    if (!req.headers[config.X_AUTHORIZATION_HEADER]) {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }
    var token = req.headers[config.X_AUTHORIZATION_HEADER].split(' ')[1];
    var payload = jwt.decode(token, config.TOKEN_SECRET);
    // console.log("payload", payload);
    // Validate user role from token
    if (payload.sub.role !== config.USER_ROLE.ADMIN) {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }
    req.user_id = payload.sub.id;
    req.user_role = payload.sub.role;

    // req.expires_at = new Date(user.exp).getTime();

    next();
};

/**
 * Authorize user API key and session token
 */
var authorizationCheck = function(req, res, next) {
    // Check session token header exists
    if (!req.headers[config.X_AUTHORIZATION_HEADER]) {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    };


    var token = req.headers[config.X_AUTHORIZATION_HEADER].split(" ")[1];
    var payload = jwt.decode(token, config.TOKEN_SECRET);

    // Validate user role from token
    // if(payload.sub.role !== config.USER_ROLE.USER) {
    //   return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
    //                                 401, res);
    // }

    req.user_id = payload.sub.id;
    req.user_role = payload.sub.role;

    console.log("auth paylaod ", payload.sub);
    if (payload.sub.firm) {
        req.firm_id = payload.sub.firm._id;
    }
    // req.expires_at = new Date(payload.exp).getTime();

    next();
};

/**
 * Authorize only non end-user sessions with respective API key and session token.
 */
var nonEndUserAuthorization = function(req, res, next) {

    // Check session token header exists
    if (!req.headers[config.X_AUTHORIZATION_HEADER]) {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    };

    var token = req.headers[config.X_AUTHORIZATION_HEADER].split(" ")[1];
    var user = jwt.decode(token, config.TOKEN_SECRET);

    // Check correct API key is recieved in header respective to user role
    if (user.role === config.USER_ROLE.USER) {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }

    req.user_id = user.sub.id;
    req.user_role = user.sub.role;

    User.findById(req.user_id, function(err, user) {
        if (err || !user) {
            return apiResponse.sendError(apiErrors.USER.NO_SUCH_USER, null,
                401, res);
        }
        req.user = user;
        next();
    })
}

/**
 * Authorize any logged in user type with respective API key and session token.
 */
var loggedInUserAuthorization = function(req, res, next) {

    // Check session token header exists
    if (!req.headers[config.X_AUTHORIZATION_HEADER]) {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    };

    var token = req.headers[config.X_AUTHORIZATION_HEADER].split(" ")[1];
    var user = jwt.decode(token, config.TOKEN_SECRET);

    // Check correct API key is recieved in header respective to user role
    if (user.role !== config.USER_ROLE.USER && user.role === config.USER_ROLE.ADMIN) {
        console.log
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }

    req.user_id = user.sub.id;
    req.user_role = user.sub.role;

    console.log(user.sub);

    // req.firm_id = user.sub.firm._id
    // req.expires_at = new Date(user.exp).getTime();

    User.findById(req.user_id, function(err, user) {
        if (err || !user) {
            return apiResponse.sendError(apiErrors.USER.NO_SUCH_USER, null,
                401, res);
        }
        req.user = user;

        if(user.access) {
            let selection = {};
            if(user.access.locations) {
                selection["node.location_flat"] = {$in: user.access.locations}
            } else if(user.access.nodes) {
                let nodeList = user.access.nodes.map((id) => {
                    return ObjectId(id);
                });
                selection["node._id"] = {$in: nodeList};
            }
            req.selection = selection;
            console.log("+++++++selection ", selection);
        }

        next();
    })


}

/**
 * Get user role from request
 */
var getUserRole = function(req, res, next) {
    if (!req.headers[config.X_AUTHORIZATION_HEADER]) {
        req.user_role = config.USER_ROLE.GUEST
    } else {
        var token = req.headers[config.X_AUTHORIZATION_HEADER].split(" ")[1];
        var user = jwt.decode(token, config.TOKEN_SECRET);
        req.user_id = user.sub.id;
        req.user_role = user.sub.role;
    }
};

var validateAPIKey = function(req, res, next) {
    if (!req.headers[config.API_KEY_HEADER_NAME] ||
        req.headers[config.API_KEY_HEADER_NAME] !== config.API_KEY) {
        return apiResponse.sendError(apiErrors.APPLICATION.INVALID_API_KEY,
            null, 401, res);
    }
    next();
}


var superAdminAuthorization = function(req, res, next) {
    // Check session token header exists
    if (!req.headers[config.X_AUTHORIZATION_HEADER]) {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }
    var token = req.headers[config.X_AUTHORIZATION_HEADER].split(' ')[1];
    var payload = jwt.decode(token, config.TOKEN_SECRET);
    // console.log("payload", payload);
    // Validate user role from token
    if (payload.sub.role === config.USER_ROLE.SUPER_ADMIN) {
        req.user_id = payload.sub.id;
        req.user_role = payload.sub.role;

        // req.expires_at = new Date(user.exp).getTime();

        next();
    } else {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }
};


var reviewerAdminAuthorization = function(req, res, next) {
    // Check session token header exists
    if (!req.headers[config.X_AUTHORIZATION_HEADER]) {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }
    var token = req.headers[config.X_AUTHORIZATION_HEADER].split(' ')[1];
    var payload = jwt.decode(token, config.TOKEN_SECRET);
    console.log("payload", payload);
    // Validate user role from token
    if (payload.sub.role === config.USER_ROLE.REVIEWER) {
        req.user_id = payload.sub.id;
        req.user_role = payload.sub.role;
        // req.expires_at = new Date(user.exp).getTime();
        next();

    } else {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }

};


var l4AdminAuthorization = function(req, res, next) {
    // Check session token header exists
    if (!req.headers[config.X_AUTHORIZATION_HEADER]) {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }
    var token = req.headers[config.X_AUTHORIZATION_HEADER].split(' ')[1];
    var payload = jwt.decode(token, config.TOKEN_SECRET);
    console.log("payload", payload);
    // Validate user role from token
    if (payload.sub.role === config.USER_ROLE.L4) {
        req.user_id = payload.sub.id;
        req.user_role = payload.sub.role;
        // req.expires_at = new Date(user.exp).getTime();
        next();

    } else {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }

};


var l3AdminAuthorization = function(req, res, next) {
    // Check session token header exists
    if (!req.headers[config.X_AUTHORIZATION_HEADER]) {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }
    var token = req.headers[config.X_AUTHORIZATION_HEADER].split(' ')[1];
    var payload = jwt.decode(token, config.TOKEN_SECRET);
    console.log("payload", payload);
    // Validate user role from token
    if (payload.sub.role === config.USER_ROLE.L3) {
        req.user_id = payload.sub.id;
        req.user_role = payload.sub.role;
        // req.expires_at = new Date(user.exp).getTime();
        next();

    } else {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }

};

var l2AdminAuthorization = function(req, res, next) {
    // Check session token header exists
    if (!req.headers[config.X_AUTHORIZATION_HEADER]) {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }
    var token = req.headers[config.X_AUTHORIZATION_HEADER].split(' ')[1];
    var payload = jwt.decode(token, config.TOKEN_SECRET);
    console.log("payload", payload);
    // Validate user role from token
    if (payload.sub.role === config.USER_ROLE.L2) {
        req.user_id = payload.sub.id;
        req.user_role = payload.sub.role;
        // req.expires_at = new Date(user.exp).getTime();
        next();

    } else {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }

};

var l1AdminAuthorization = function(req, res, next) {
    // Check session token header exists
    if (!req.headers[config.X_AUTHORIZATION_HEADER]) {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }
    var token = req.headers[config.X_AUTHORIZATION_HEADER].split(' ')[1];
    var payload = jwt.decode(token, config.TOKEN_SECRET);
    console.log("payload", payload);
    // Validate user role from token
    if (payload.sub.role === config.USER_ROLE.L1) {
        req.user_id = payload.sub.id;
        req.user_role = payload.sub.role;
        // req.expires_at = new Date(user.exp).getTime();
        next();

    } else {
        return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_REQUEST, null,
            401, res);
    }

};

module.exports = {
    adminAuthorization: adminAuthorization,
    nonEndUserAuthorization: nonEndUserAuthorization,
    authorizationCheck: authorizationCheck,
    loggedInUserAuthorization: loggedInUserAuthorization,
    getUserRole: getUserRole,
    validateAPIKey: validateAPIKey,
    superAdminAuthorization: superAdminAuthorization,
    reviewerAdminAuthorization: reviewerAdminAuthorization,
    l4AdminAuthorization: l4AdminAuthorization,
    l3AdminAuthorization: l3AdminAuthorization,
    l2AdminAuthorization: l2AdminAuthorization,
    l1AdminAuthorization: l1AdminAuthorization,
}