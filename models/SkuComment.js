'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
  sku: {},
  comment: {type: String}
};

var SkuCommentSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('SkuComment', SkuCommentSchema);