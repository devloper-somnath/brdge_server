'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
    
    title: {
        type: String
    },
    values: []
   
    
};

var SkuAttributeSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('SkuAttribute', SkuAttributeSchema);