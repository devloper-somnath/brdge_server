'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    moment = require('moment');

var fields = {
  total_price: {type: Number, default: 0},
  total: {type: Number, default: 0},
  location: {},
  node: {},
  skus_by_price: [],
  skus_by_quantity: [],
  date: { type: Date, default: moment().startOf('day').date() }
};

var BestPerformingNodeSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('BestPerformingNode', BestPerformingNodeSchema);
