'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
   // ObjectId = Schema.ObjectId;

var fields = {
    name: {
        type: String
    },
    meta:{},
    skucode : {
        type:String
    },
    price :Number,
    comment_count: {type: Number, default: 0}
    
};

var SKUMasterSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('SKUMaster', SKUMasterSchema);