'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
    stock : {},
    skucode: {
        type: String
    },
    sku:{},
    node:{},
    buyer: {},
    stock_norm:Number,
    actual_stock : Number,
    availability_zone :Number,
    days_seen_last_changed :Number,
    comment_count: {type: Number, default: 0}
    
};

var NodeSKUMapSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('NodeSKUMap', NodeSKUMapSchema);