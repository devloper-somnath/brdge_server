'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
    
    designation: {
        type: String
    },
    role: {
        type: String
    },
    parent: {
        type: String
    },
   
   nodes: [],
   locations :[],
   skus : []
    
};

var UserRoleSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('UserRole', UserRoleSchema);