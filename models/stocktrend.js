'use strict';

//Dailysns schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
    node : {},
    sku : {},
    stock_norm : Number ,
    stock : [] 
    
};

var StockTrendSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('StockTrend', StockTrendSchema);