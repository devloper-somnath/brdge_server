'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
    name: {
        type: String
    },
    parent:{}
    
};

var SKUMetaSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('SKUMeta', SKUMetaSchema);