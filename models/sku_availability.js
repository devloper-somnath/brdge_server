'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
    location: {},
    sku:{},
    black:Number,
    red:Number,
    yellow:Number,
    green:Number,
    blue:Number,
    days_in_black : Number ,
    days_in_red : Number ,
    days_in_yellow : Number ,
    days_in_green : Number ,
    days_in_blue : Number
    
   
    
};

var SkuAvailabilitySchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('SkuAvailability', SkuAvailabilitySchema);