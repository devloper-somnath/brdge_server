'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
    sku: {},
    supplier: {},
    consumer: {},
    stocknorm: {type : Number},
    actual_stock: {type : Number},
    date: {type : Date},
    availability_zone: {type : Number}
};

var SNSTrendSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('SNSTrend', SNSTrendSchema);