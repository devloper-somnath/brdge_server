'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
  nodesku: {},
  comment: {type: String}
};

var NodeSkuCommentSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('NodeSkuComment', NodeSkuCommentSchema);
