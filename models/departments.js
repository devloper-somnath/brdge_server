'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
    name: {
        type: String
    }
    
};

var DepartmentSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('Department', DepartmentSchema);