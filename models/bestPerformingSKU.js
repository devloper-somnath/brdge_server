'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    moment = require('moment');

var fields = {
  total_price: {type: Number, default: 0},
  total: {type: Number, default: 0},
  location: {},
  sku: {},
  nodes_by_quantity: [],
  nodes_by_price: [],
  date: { type: Date, default: moment().startOf('day').date() }
};

var BestPerformingSkuSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('BestPerformingSku', BestPerformingSkuSchema);
