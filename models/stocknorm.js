'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
    supplier_sku_code: String , 
    consumer_sku_code: String ,
    supply_quantity: Number,
    actual_stock : Number ,
    actual_quantity : Number,
    stock_norm : Number,
    date : {
        type: Date,
        default: Date.now
    },
    in_transite_stock: Number,
    skumaster: {},
    supplier:{},
    consumer:{} ,
    availability_zone :{
        type:Number
    },
    days_seen_last_changed :Number,
    stock:{},
};

var stocknormSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('StockNorm', stocknormSchema);