'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
    name: {
        type: String
    },
    parent:{},
    black :Number,
    red : Number,
    yellow : Number,
    green : Number,
    blue : Number,
    black_price : Number,
    red_price : Number,
    yellow_price : Number,
    green_price : Number,
    blue_price : Number
    
};

var LocationSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('Location', LocationSchema);