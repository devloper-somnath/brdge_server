'use strict';

//Services schema model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fields = {
    supplier_sku_code:String,   
    child_sku_code:String,
    supply_quantity:Number,
    skumaster:{},
    supplier:{},
    child:{}, 
    stock: {}
};

var NodeMapSchema = new Schema(fields, { timestamps: true });

module.exports = mongoose.model('NodeMap', NodeMapSchema);