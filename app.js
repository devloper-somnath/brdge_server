'use strict';

// Module dependencies.
var express = require('express'),
    cors = require('cors'),
    path = require('path'),
    fs = require('fs'),
    methodOverride = require('method-override'),
    morgan = require('morgan'),
    errorhandler = require('errorhandler'),
    http = require('http'),
    config = require('./config/config'),
    apiAuth = require('./utils/apiAuth'),
    mongoose = require('mongoose');

var app = module.exports = exports.app = express();

var server = http.createServer(app);

app.locals.siteName = "Finswell";

app.use(cors());


var uploadDir = './uploads';
//Create uploa dir if not exist
if (!fs.existsSync(uploadDir)) {
    fs.mkdirSync(uploadDir);
}

// Connect to database
var db = require('./config/db');
app.use(express.static(__dirname + '/public'));


// Bootstrap models
var modelsPath = path.join(__dirname, 'models');
fs.readdirSync(modelsPath).forEach(function (file) {
  require(modelsPath + '/' + file);
});

var env = process.env.NODE_ENV || 'development';

if ('development' == env) {
    app.use(morgan('dev'));
    app.use(errorhandler({
        dumpExceptions: true,
        showStack: true
    }));
    app.set('view options', {
        pretty: true
    });
}

if ('production' == env) {
    app.use(morgan());
     app.use(errorhandler({
        dumpExceptions: false,
        showStack: false
    }));
}

app.use(methodOverride());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(apiAuth.validateAPIKey);

// Bootstrap routes
var routesPath = path.join(__dirname, 'routes');
fs.readdirSync(routesPath).forEach(function(file) {
  app.use('/', require(routesPath + '/' + file));
});

// Bootstrap api
var apiPath = path.join(__dirname, 'api');
fs.readdirSync(apiPath).forEach(function(file) {
    app.use('/api', require(apiPath + '/' + file));
});

// Start server
var port = process.env.PORT || 3000;
server.listen(port, function () {
    console.log('Express server listening on port %d in %s mode', port, app.get('env'));

    Node = mongoose.models.Node;
    Location = mongoose.models.Location;

    Node.find(function(err, nodes) {
      for(let index = 0; index < nodes.length; ++index) {
        const currentNode = nodes[index];
        let location = currentNode.location;
        currentNode.location_flat = [];
        do {
          currentNode.location_flat.splice(0, 0, location._id.toString())
          location = location.parent
        } while(location);
        currentNode.save(function(err) {
          console.log("+++++++ updated node ", currentNode.name);
        });
      }
    });
});
