'use strict';
var mongoose = require('mongoose'),
	bluebird = require('bluebird');
//BRDge
  var db_name = process.env.DB_NAME || "BRDge";
  var db_host = process.env.DB_HOST || "localhost";
  var db_username = process.env.DB_USER || "";
  var db_password = process.env.DB_PASSWORD || "";
  var db_port = process.env.DB_PORT || "27017";

  var config = {
    "db": db_name,
    "host": db_host,
    "user": db_username,
    "pw": db_password,
    "port": db_port
  };

var port = (config.port.length > 0) ? ":" + config.port : '';
var login = (config.user.length > 0) ? config.user + ":" + config.pw + "@" : '';
var uristring =  "mongodb://" + login + config.host + port + "/" + config.db;

var mongoOptions = { db: { safe: true },
  					 promiseLibrary: bluebird
				 	};

// Connect to Database
mongoose.connect(uristring, mongoOptions, function (err, res) {
  if(err){
    console.log('ERROR connecting to: ' + uristring + '. ' + err);
  }else{
    console.log('Successfully connected to: ' + uristring);
  }
});


exports.mongoose = mongoose;
