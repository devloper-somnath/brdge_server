module.exports = {
    // App Settings
    MONGO_URI: process.env.MONGO_URI || 'localhost',

    //API key info
    API_KEY_HEADER_NAME: 'api_key',
    API_KEY: process.env.API_KEY || 'n283rfn338nvr234t',

    // Request timeout (milliseconds)
    REQUEST_TIMEOUT: 300000, // 5 min

    //Payload checksum header key
    PAYLOAD_CHECKSUM_HEADER_NAME: 'payload_checksum',

    // Session token header key
    X_AUTHORIZATION_HEADER: 'authorization',

    // JWT encryption salt
    TOKEN_SECRET: 'mongoose_project',

    // Email client smtp config
    poolConfig: {
        pool: true,
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // use SSL
        auth: {
            user: 'testingemail12341234@gmail.com',
            pass: 'testingemail'
        }
    },

    // Log file storage location
    logPath: 'log/app.log',

    // Log status
    LOG_STATUS: {
        SUCCESS: 'SUCCESS',
        FAILURE: 'FAILURE'
    },

    // User role
    USER_ROLE: {
        SUPER_ADMIN: 'super_admin',
        USER: 'user',
        ADMIN: 'admin',
        REVIEWER: 'reviewer',
        L4: 'l4',
        L3: 'l3',
        L2: 'l2',
        L1: 'l1'
    },

    //content types

    CONTENT_TYPES: {
        IMAGE: 0,
        SLIDE_SHOW: 1,
        AUDIO: 2,
        VIDEO: 3,
        DOCUMENT: 4,
        STORY: 5,
        CONTENT: 6
    },

    // Client website base URL
    WEBSITE_BASE_URL: 'http://test.com',

    // Email verification page relative path (path after above base URL)
    WEBSITE_EMAIL_VERIFICATION_API: '/user/emailverify',


    AWS_KEY_ID: "SOMEKEY",
    AWS_SECRET: "SOMESECRET",
    AWS_BUTCKET: "windham",


    // Credimax payment gateway configuration
    VPCURL: "https://migs.mastercard.com.au/vpcpay",
    // Replace with your merchant key from credimax
    MERCHANT: "EXXXXXXX",

    // Replace this with your merchant key  from credimax
    ACCESS_CODE: "XXXXXXXX",

    // Replace this with your secure secret from credimax
    SECURE_SECRET: "Pravinvegare"
};