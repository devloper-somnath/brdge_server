// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    SkuAttribute = mongoose.models.SkuAttribute,
    
    ///User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

    // mongoose.set('debug', true);


//display all skuattributee

  const allSkuAttribute = function (req,res) {
    SkuAttribute.find(apiResponse.getStandardDBResponseHandler(res, true))
  }






// GET 
// Retrive single skuattribute information
  const getSkuAttribute = function(req, res) {
    SkuAttribute.findById(req.params.id,
      apiResponse.getStandardDBResponseHandler(res, true));
  };

// POST
// Add a skuattribute information

  const addSkuAttribute =   function(req, res) {
      console.log("req.body++++++",req.body)
    let skuattribute = new SkuAttribute(req.body);
  
    skuattribute.save(apiResponse.getStandardDBResponseHandler(res, true));
      
  };

// PUT
// Edit skuattribute information
  const editSkuAttribute = function(req, res) {
    var id = req.params.id;
    SkuAttribute.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
  };


// DELETE
// Delete skuattribute information
  const deleteSkuAttribute = function(req, res) {
    var id = req.params.id;
    return SkuAttribute.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));
  };


 
//api/all skuattribute
  router.get('/allskuattributes',allSkuAttribute)


// POST /api/skuattribute
// To add a skuattribute information
  router.post('/skuattribute', addSkuAttribute);

// GET,PUT And DELETE /api/skuattribute/:id
// To get,update and delete a skuattribute by using _id
router.route('/skuattribute/:id')
    .get(getSkuAttribute)
    .put(editSkuAttribute)
    .delete(deleteSkuAttribute);



module.exports = router;