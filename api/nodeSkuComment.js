// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    Location = mongoose.models.Location,
    StockNorm = mongoose.models.StockNorm,
    NodeSKUMap = mongoose.models.NodeSKUMap,
    NodeSkuComment = mongoose.models.NodeSkuComment,
    ///User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

    // mongoose.set('debug', true);



const getComments = function(req, res) {
  NodeSkuComment.find(
    {"nodesku._id": ObjectId(req.params.id)}, {}, {sort: {createdAt: -1}},
    apiResponse.getStandardDBResponseHandler(res, true));
}

const getCommentsByIds = function(req, res) {
  NodeSkuComment.find(
    {"nodesku.node._id": ObjectId(req.params.node), "nodesku.sku._id": ObjectId(req.params.sku)},
    {}, {sort: {createdAt: -1}},
    apiResponse.getStandardDBResponseHandler(res, true));
}

const postComment = function(req, res) {
    // console.log("node sku id is ",req.params.id)
  NodeSKUMap.findById(req.params.id, function(err, result) {
    let newComment = new NodeSkuComment(req.body);
    newComment.nodesku = result;
    newComment.save(apiResponse.getStandardDBResponseHandler(res, false));
    if(result.comment_count) {
      result.comment_count++;
      
    } else {
      result.comment_count = 1;
    }
    result.save(function(){ console.log("count updated"); });
  });

}


const postCommentByIds = function(req, res) {
  // console.log("node sku id is ",req.params.id)
  NodeSKUMap.findOne({"node._id": ObjectId(req.params.node), "sku._id": ObjectId(req.params.sku)}, 
    function(err, result) {
      let newComment = new NodeSkuComment(req.body);
      newComment.nodesku = result;
      newComment.save(apiResponse.getStandardDBResponseHandler(res, false));
      if(result.comment_count) {
        result.comment_count++;
        
      } else {
        result.comment_count = 1;
      }
      result.save(function(){ console.log("count updated"); });
    }
  );

}

// NodeSkuComment.findById(id,function(err,commentresult)  {
    // let nodeskuid = commentresult.nodesku._id;
      
    //   NodeSKUMap.update({"_id":nodeskuid},
    //   {$inc:{comment_count:-1}},function(err,sucess) {
    //     NodeSkuComment.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));
    //     })
    //   });

// DELETE
// Delete location information
  const deleteComment = function(req, res) {
    var id = req.params.id;
   
    NodeSkuComment.findById(id).lean().exec(function(err,commentresult)  {
      
    let nodeskuid = commentresult.nodesku._id;
    
      
      NodeSKUMap.update({"_id":nodeskuid},
      {$inc:{comment_count:-1}},function(err,sucess) {
        NodeSkuComment.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));
        })
      });
  }


  router.get('/comments/nodesku/:id', getComments);
  router.get('/comments/nodesku/:node/:sku', getCommentsByIds);
  router.post('/comments/nodesku/:id', postComment);
  router.post('/comments/nodesku/:node/:sku', postCommentByIds);
  router.delete('/comments/nodesku/:id', deleteComment);



module.exports = router;
