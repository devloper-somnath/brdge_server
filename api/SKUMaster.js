// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    SKUMaster = mongoose.models.SKUMaster,
    SKUMeta =mongoose.models.SKUMeta;
    ///User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

// mongoose.set('debug', true);

// ALL
const getallSKUMaster = function(req, res) {
    SKUMaster.find(apiResponse.getStandardDBResponseHandler(res, true));
};

// GET 
// Retrive single sku information
const getSKUMaster = function(req, res) {
    SKUMaster.findById(req.params.id,
        apiResponse.getStandardDBResponseHandler(res, true));
};

// POST
// Add a sku details into database
const addSKUMaster = function(req, res) {
    
    let skumaster = new SKUMaster(req.body);
    SKUMeta.findById(req.body.meta,(err,result)=> {
        skumaster.meta = result
        skumaster.save(apiResponse.getStandardDBResponseHandler(res, false));
    })
   
};

// PUT
// Edit sku information
const editSKUMaster = function(req, res) {
    var id = req.params.id;
    SKUMaster.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
};


// DELETE
// Delete a sku from database
const deleteSKUMaster = function(req, res) {
    var id = req.params.id;
    return SKUMaster.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));

};

// GET /api/SKUMaster
// To get all details of sku 
router.get('/skumasters', getallSKUMaster);

// POST /api/SKUMaster
// To add a information into sku table
router.post('/skumaster', addSKUMaster);

// GET,PUT And DELETE /api/SKUMaster/:id
// To get,update and delete a sku by using _id
router.route('/skumaster/:id')
    .get(getSKUMaster)
    .put(editSKUMaster)
    .delete(deleteSKUMaster);



module.exports = router;