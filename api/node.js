// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    Node = mongoose.models.Node,
    Location = mongoose.models.Location,
    Department = mongoose.models.Department
    ///User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

    // mongoose.set('debug', true);

// ALL
    const getallNode = function(req, res) {
        let selection = {};
        if(req.query.location) {
            selection["location_flat"] = req.query.location;
        }
        Node.find(selection, apiResponse.getStandardDBResponseHandler(res, true));
    };

// GET 
// Retrive mode information
    const getNode = function(req, res) {
        Node.findById(req.params.id,
        apiResponse.getStandardDBResponseHandler(res, true));
};

// POST
// Add a Node information
    const addNode =  function(req, res) {
        let node = new Node(req.body);
        if(req.body.location_id) { 
            Location.findById(req.body.location_id,(err,result)=>{
            node.location = result;
            Department.findById(req.body.department_id,(err,result)=>{
            node.department = result;
            node.save(apiResponse.getStandardDBResponseHandler(res, true));
        })
        })
        }
    };

// PUT
// Edit node location
    const editNode = function(req, res) {
        var id = req.params.id;
        Node.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
    };


// DELETE
// Delete a Node information
    const deleteNode = function(req, res) {
        var id = req.params.id;
        return Node.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));
    };

// GET /api/Node
// To get all nodes details
    router.get('/nodes', getallNode);

// POST /api/Node
// To add a node inforation
    router.post('/node', addNode);

// GET,PUT And DELETE /api/Node/:id
// To get,update and delete a node by using _id
    router.route('/node/:id')
    .get(getNode)
    .put(editNode)
    .delete(deleteNode);



module.exports = router;