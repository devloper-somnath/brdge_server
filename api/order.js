// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    Order = mongoose.models.Order,
    SKUMaster = mongoose.models.SKUMaster,
    Node = mongoose.models.Node,
    
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

// mongoose.set('debug', true);

// ALL
const getallorders = function(req, res) {
    Order.find(apiResponse.getStandardDBResponseHandler(res, true));
};

// GET 
// Retrive single laundry category information
const getorder = function(req, res) {
    Order.findById(req.params.id,
        apiResponse.getStandardDBResponseHandler(res, true));
};

// POST
// Add a laundry category information
const addorder =  function(req, res) {
    let order = new Order(req.body);
    if(req.body.skumaster_id){
   SKUMaster.findById(req.body.skumaster_id,(err,result)=>{
                nodemap.skumaster = result
    Node.findById(req.body.suppliernode_id,(err,result)=>{
                    nodemap.supplier = result
    Node.findById(req.body.childnode_id,(err,result)=>{
                        nodemap.child = result
    order.save(apiResponse.getStandardDBResponseHandler(res, true));

       
               })
         })
                 
              })
      }
};

// PUT
// Edit new laundry category information
const editorder = function(req, res) {
    var id = req.params.id;
    Order.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
};


// DELETE
// Delete a laundry category information
const deleteorder = function(req, res) {
    var id = req.params.id;
    return Order.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));

};

// GET /api/Oreder
// To get all categories from laundry-category resource 
router.get('/orders', getallorders);

// POST /api/Oreder
// To add a category in laundry-category resource
router.post('/order', addorder);

// GET,PUT And DELETE /api/Oreder/:id
// To get,update and delete a category from laundry-resource by using _id
router.route('/order/:id')
    .get(getorder)
    .put(editorder)
    .delete(deleteorder);



module.exports = router;