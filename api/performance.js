// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    BestPerformingSku = mongoose.models.BestPerformingSku,
    BestPerformingNode = mongoose.models.BestPerformingNode,
    ///User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

    // mongoose.set('debug', true);


//display all location on client side

  const getBestPerformingNode = function(req, res) {
    BestPerformingNode.find(apiResponse.getStandardDBResponseHandler(res, true)).limit(5);
  }
  //.sort({total:-1})

  const getBestPerformingSKU = function(req, res) {
    BestPerformingSku.find(apiResponse.getStandardDBResponseHandler(res, true)).limit(5);
  }      //sort({total:-1}).

//get parent location
//api/all locations
  router.get('/performance/node', getBestPerformingNode);

// GET /api/location
// To get all location details
  router.get('/performance/sku', getBestPerformingSKU);

module.exports = router;
