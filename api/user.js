// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    DailyNodeLocation = mongoose.models.DailyNodeLocation,
    User = mongoose.models.User,
    Node = mongoose.models.Node,
    Location = mongoose.models.Location,
    Receipt = mongoose.models.Receipt,
    NodeSKUMap = mongoose.models.NodeSKUMap,
    Orders = mongoose.models.Orders,
    StockNorm = mongoose.models.StockNorm,
    NodeMap = mongoose.models.NodeMap,
    NodeSkuComment = mongoose.models.NodeSkuComment,
    api = {},
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),
    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config'),
    async = require("async"),
    bcrypt = require("bcryptjs"),
    admin = require('firebase-admin');


mongoose.set("debug", true);

var serviceAccount = require("../wyndham-7ff22-firebase-adminsdk-gk262-54d0d5415c.json");

admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://wyndham-7ff22.firebaseio.com"
    })
    // Fetch receipts by me
const myReceipts = function(req, res) {

    let selection = { "firm._id": ObjectId(req.firm_id) };

    if (req.query.type) {
        selection["type"] = req.query.type;
    }

    if (req.query.rejected) {
        console.log("has rejected with value ", req.query.rejected);
        selection["rejected"] = req.query.rejected;
    }

    if (req.query.approved) {
        selection["approved"] = req.query.approved;
    }

    Receipt.find(selection, {}, { sort: { createdAt: 1 } }, apiResponse.getStandardDBResponseHandler(res, true))
};

// ALL
api.users = function(req, res) {
    let selection = {};
    let options = { offset: 0 };

    if (req.query.role) {
        selection.role = req.query.role
    }

    if (req.query.page) {
        options.offset = page * 20;
    }

    User.find(selection, {}, options, apiResponse.getStandardDBResponseHandler(res, true));
};

// GET
api.user = function(req, res) {
    var id = req.params.id;
    User.findOne({ '_id': id }, function(err, user) {
        if (err) {
            return apiResponse.sendError(apiErrors.APPLICATION.INTERNAL_ERROR, null, 400, res);
        } else {
            return apiResponse.sendResponse(user, 200, res);
        }
    });
};

api.getprofile = function(req, res) {
    var id = req.user_id;
    User.findOne({ '_id': id }, function(err, user) {
        if (err) {
            return apiResponse.sendError(apiErrors.APPLICATION.INTERNAL_ERROR, null, 400, res);
        } else {
            return apiResponse.sendResponse(user.getMinorDetails, 200, res);
        }
    });
};

// POST
api.addUser = function(req, res) {
    console.log(req.body);
    let user = new User(req.body);
    user.role = config.USER_ROLE.STAFF;
    user.password = "1234";
    user.save(function(err) {
        if (!err) {
            console.log("created user");
            return apiResponse.sendResponse(user.toObject(), 200, res);
        } else {
            return apiResponse.sendError(apiErrors.APPLICATION.INTERNAL_ERROR, null, 500, res);
        }
    });
};

const userSignup = function(req, res) {
    User.findOne({ email: req.body.email }, function(userErr, existingUser) {
        if (!existingUser) {

            admin.auth().createUser({
                    email: req.body.email,
                    emailVerified: false,
                    phoneNumber: req.body.phone,
                    password: req.body.password,
                    displayName: req.body.name,
                    photoURL: "",
                    disabled: false
                })
                .then(function(userRecord) {
                    let newUser = new User(req.body);
                    newUser.fcm_id = userRecord.uid;
                    newUser.save(apiResponse.getStandardDBResponseHandler(res, false));
                })
                .catch(function(error) {
                    console.log('Error creating new user:', error);
                });

        } else {

        }
    });
};

// PUT
api.editUser = function(req, res) {
    var id = req.params.id;

    User.findById(id, function(err, user) {

        if (typeof req.body.user["name"] != 'undefined') {
            user["name"] = req.body.user["name"];
        }

        if (typeof req.body.user["email"] != 'undefined') {
            user["email"] = req.body.user["email"];
        }

        if (typeof req.body.user["password"] != 'undefined') {
            user["password"] = req.body.user["password"];
        }

        if (typeof req.body.user["is_verified"] != 'undefined') {
            user["is_verified"] = req.body.user["is_verified"];
        }

        if (typeof req.body.user["role"] != 'undefined') {
            user["role"] = req.body.user["role"];
        }

        if (typeof req.body.user["last_login"] != 'undefined') {
            user["last_login"] = req.body.user["last_login"];
        }

        if (typeof req.body.user["is_logged_in"] != 'undefined') {
            user["is_logged_in"] = req.body.user["is_logged_in"];
        }


        return user.save(function(err) {
            if (!err) {
                console.log("updated user");
                return apiResponse.sendResponse(user.toObject(), 200, res);
            } else {
                return apiResponse.sendError(apiErrors.APPLICATION.INTERNAL_ERROR, null, 500, res);
            }
            return apiResponse.sendResponse(user, 200, res);
        });
    });

};

// DELETE
api.deleteUser = function(req, res) {

    var id = req.params.id;
    // console.log(id);
    // if (req.user_role === config.USER_ROLE.USER &&
    //     req.user_id !== id) {
    //     return apiResponse.sendError(apiErrors.APPLICATION.UNAUTHORIZED_ERROR, null,
    //         401, res);
    // }

    return User.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));

};

const getAllUserOrders = function(req, res) {
    let selection = {}

    if (req.user_id) {
        selection["user"] = req.user_id
    }
    if (req.query.status) {
        selection["status"] = req.query.status
    }

    if (req.query.type) {
        selection["type"] = req.query.type
    }

    Orders.find(selection, apiResponse.getStandardDBResponseHandler(res, true));
}


const getDashboardInfo = function(req, res) {
    async.parallel({
            summary: function(callback) {
                getDashboardSummary(req, callback);
            },
            stock: function(callback) {
                getDashboardStockStatus(req, callback);
            },
            history: function(callback) {
                getDashboardMonthlyOverallStatus(req, callback);
            }
        }, 
        apiResponse.getStandardDBResponseHandler(res, true)
    );


};

const getUserStocknorm = function(req, res) {
    let selection = {};
    console.log("\n\n\nType in req  query ", req.query.type);
    if(req.query.type == '1') {
        selection['consumer._id'] = ObjectId(req.query.parent_id);
    } else {
        if(req.selection['node.location_flat']) {
            if(req.query.parent_id) {
                selection['consumer.location_flat'] = req.query.parent_id;
            } else {
                selection['consumer.location_flat'] = req.selection['node.location_flat'];
            }
        } else if(req.selection['node._id']) {
            if(req.query.parent_id) {
                selection['consumer.location_flat'] = req.query.parent_id;
            }
            selection['consumer._id'] = req.selection['node._id'];
        }
        
    }
    StockNorm.find(selection, {}, {lean: true},
        function(err, result) {
            async.each(result, function(item, callback) {
                NodeSkuComment.count(
                    {"nodesku.node._id": ObjectId(item.consumer._id), "nodesku.sku._id": ObjectId(item.skumaster.sku._id)},
                    function(commenterr, count) { 
                        if(count && count > 0) {
                            console.log("########## count is ", count);
                        }
                        item.comment_count = count; 
                        callback();
                    } );
            }, function(err) {
                apiResponse.sendResponse(result, 200, res);
            });
        });
};

const getUserAccessibleLocation = function(req, res) {
    let selection = {};
    if(req.selection['node.location_flat']) {
        console.log(req.query);
        if(req.query.parent_id != null && req.query.parent_id != 'null') {
            selection['location_flat'] = req.query.parent_id;
        } else {
            selection['location_flat'] = req.selection['node.location_flat'];
        }
    } else if(req.selection['node._id']) {
        selection['_id'] = req.selection['node._id'];
        if(req.query.parent_id != null && req.query.parent_id != 'null') {
            selection['location_flat'] = req.query.parent_id;
        }
    }

    let projection = "location_flat." + (req.query.level ? req.query.level : '1');
    let result = [];
    Node.distinct(projection, selection, function(err, list) {
        if(list && list.length > 0) {
            async.each(list, function(item, callback) {
                Location.findById(item, function(locationErr, location) {
                    result.push({label: location.name, value: location._id, type: 0});
                    callback();
                });
            }, function(asyncErr) {
                apiResponse.sendResponse(result, 200, res);
            });
        } else {
            Node.find(selection, function(nodesErr, nodes) {
                async.each(nodes, function(item, callback) {
                    result.push({label: item.name, value: item._id, type: 1});
                    callback();
                }, function(asyncErr) {
                    apiResponse.sendResponse(result, 200, res);
                });
            })
        }
            
    });
};


const getNodeSku = function (req,res) {
    let selection = {...req.selection}
    // if(req.selection['node.location_flat']) {
    //     selection['node.location_flat'] = req.selection['node.location_flat'];
    // } else if(req.selection['node._id']) {
    //     selection['_id'] = req.selection['node._id'];
    // }
    if(req.query.availability_zone!=null) {
        selection['availability_zone'] = req.query.availability_zone
    }
    NodeSKUMap.find(selection,apiResponse.getStandardDBResponseHandler(res, true)).sort({ days_seen_last_changed: -1 });
}

const getConsumerTrend = function(req, res) {
    let selection = {"child._id": ObjectId(req.params.id)};
    NodeMap.find(selection, {}, {lean: true}, function(err, result) {
        async.each(result, function(item, callback) {
            async.parallel([
                function(innerCallback) {
                    SNSTrend.find({"sku.sku._id": item.skumaster.sku._id, 
                    "supplier._id": item.supplier._id, "consumer._id": item.child._id},
                    {}, {sort: {date: -1}}, function(err, dateData) {
                        item.stock = dateData;
                        innerCallback();
                    });
                },
                function(innerCallback) {
                    NodeSkuComment.count(
                        {"nodesku.node._id": ObjectId(item.child._id), "nodesku.sku._id": ObjectId(item.skumaster.sku._id)},
                        function(commenterr, count) { 
                            if(count && count > 0) {
                                console.log("########## count is ", count);
                            }
                            item.comment_count = count; 
                            innerCallback();
                        });
                }
            ], function() {
                callback();
            });
        }, function(asyncErr) {
            apiResponse.sendResponse(result, 200, res);
        });
    });

    // NodeMap.find(selection, {}, {lean: true}, apiResponse.getStandardDBResponseHandler(res, true));
};

const getSKUTrend = function(req, res) {
    let selection = {"skumaster.sku._id": ObjectId(req.params.id)};
    NodeMap.find(selection, {}, {lean: true}, function(err, result) {
        async.each(result, function(item, callback) {
            async.parallel([
                function(innerCallback) {
                    SNSTrend.find({"sku.sku._id": item.skumaster.sku._id, 
                    "supplier._id": item.supplier._id, "consumer._id": item.child._id},
                    {}, {sort: {date: -1}}, function(err, dateData) {
                        item.stock = dateData;
                        innerCallback();
                    });
                },
                function(innerCallback) {
                    NodeSkuComment.count(
                        {"nodesku.node._id": ObjectId(item.child._id), "nodesku.sku._id": ObjectId(item.skumaster.sku._id)},
                        function(commenterr, count) { 
                            if(count && count > 0) {
                                console.log("########## count is ", count);
                            }
                            item.comment_count = count; 
                            innerCallback();
                        });
                }
            ], function() {
                callback();
            });
        }, function(asyncErr) {
            apiResponse.sendResponse(result, 200, res);
        });
    });
    // NodeMap.find(selection, {}, {lean: true}, apiResponse.getStandardDBResponseHandler(res, true));
};

const getSupplierTrend = function(req, res) {
    let selection = {"supplier._id": ObjectId(req.params.id)};
    NodeMap.find(selection, {}, {lean: true}, function(err, result) {
        async.each(result, function(item, callback) {
            
            async.parallel([
                function(innerCallback) {
                    SNSTrend.find({"sku.sku._id": item.skumaster.sku._id, 
                    "supplier._id": item.supplier._id, "consumer._id": item.child._id},
                    {}, {sort: {date: -1}}, function(err, dateData) {
                        item.stock = dateData;
                        innerCallback();
                    });
                },
                function(innerCallback) {
                    NodeSkuComment.count(
                        {"nodesku.node._id": ObjectId(item.child._id), "nodesku.sku._id": ObjectId(item.skumaster.sku._id)},
                        function(commenterr, count) { 
                            if(count && count > 0) {
                                console.log("########## count is ", count);
                            }
                            item.comment_count = count; 
                            innerCallback();
                        });
                }
            ], function() {
                callback();
            });

            
        }, function(asyncErr) {
            apiResponse.sendResponse(result, 200, res);
        });
    });
    // NodeMap.find(selection, {}, {lean: true}, apiResponse.getStandardDBResponseHandler(res, true));
};

/** 
 * Private methods 
 * */

const getDashboardSummary = function(req, callback) {
    let selection = {...req.selection};
    if(req.selection['node.location_flat']) {
        if(req.query.parent_id) {
            selection["$and"] = [ 
                {'node.location_flat': req.query.parent_id},
                {'node.location_flat': req.selection['node.location_flat']}
            ];
        } else {
            selection['node.location_flat'] = req.selection['node.location_flat'];
        }
    } else if(req.selection['node._id']) {
        if(req.query.parent_id) {
            selection['node.location_flat'] = req.query.parent_id;
        }
        selection['node._id'] = req.selection['node._id'];
    }
    console.log("@@@@@@before query ", selection);

    selection = [
        {
            "$match": selection
        },
        {
            "$group": {
                "_id": null,
                "blue": {
                    "$sum": "$blue"
                    },
                "green": {
                    "$sum": "$green"
                    },
                "yellow": {
                    "$sum": "$yellow"
                    },
                "red": {
                    "$sum": "$red"
                    },
                "black": {
                    "$sum": "$black"
                    },
                "blue_price": {
                    "$sum": "$blue_price"
                    },
                "green_price": {
                    "$sum": "$green_price"
                    },
                "yellow_price": {
                    "$sum": "$yellow_price"
                    },
                "red_price": {
                    "$sum": "$red_price"
                    },
                "black_price": {
                    "$sum": "$black_price"
                    }      
                }
        }
    ];

    console.log("+++++++++ selection in request", JSON.stringify(selection));
    DailyNodeLocation.aggregate(selection) 
    .exec(
            function(err, result) {
                console.log("err1", err);
                console.log("result1", result);
                callback(err, result);
        
        }
    );
};

const getDashboardStockStatus = function(req, callback) {
    let selection = {...req.selection};

    if(req.selection['node.location_flat']) {
        if(req.query.parent_id) {
            selection["$and"] = [ 
                {'node.location_flat': req.query.parent_id},
                {'node.location_flat': req.selection['node.location_flat']}
            ];
        } else {
            selection['node.location_flat'] = req.selection['node.location_flat'];
        }
    } else if(req.selection['node._id']) {
        if(req.query.parent_id) {
            selection['node.location_flat'] = req.query.parent_id;
        }
        selection['node._id'] = req.selection['node._id'];
    }

    selection = [
        {
            '$match': selection
        },
        {
            '$group': {
                _id: { $arrayElemAt: [ "$node.location_flat", parseInt(req.query.level) ] },
                blue: {
                    $sum: "$blue"
                    },
                green: {
                    $sum: "$green"
                    },
                yellow: {
                    $sum: "$yellow"
                    },
                red: {
                    $sum: "$red"
                    },
                black: {
                    $sum: "$black"
                    },
                blue_price: {
                    $sum: "$blue_price"
                    },
                green_price: {
                    $sum: "$green_price"
                    },
                yellow_price: {
                    $sum: "$yellow_price"
                    },
                red_price: {
                    $sum: "$red_price"
                    },
                black_price: {
                    $sum: "$black_price"
                    }   
                }
        }
    ];

    DailyNodeLocation.aggregate(selection) 
    .exec(
        function(err, result) {
            if(result.length == 1 && null == result[0]._id) {
                selection[1]['$group']._id = '$node._id';
                DailyNodeLocation.aggregate(selection) 
                .exec(
                    function(err, result) {
                        console.log("Result for nodes", result);
                        async.each(result, function(record, innerCallback) {
                            Node.findById(record._id, function(nodeErr, node) {
                                record.name = node.name;
                                innerCallback();
                            });
                        }, function(innerErr) {
                            callback(innerErr, result);
                        });
                    });
                return;
            }

            async.each(result, function(record, innerCallback) {
                if(null != record._id) {
                    Location.findById(record._id, function(locationErr, location) {
                        if(location) {
                            record.name = location.name;
                            innerCallback();
                        } else {
                            Node.findById(record._id, function(nodeErr, node) {
                                record.name = node.name;
                                innerCallback();
                            });
                        }
                    });
                } else {
                    innerCallback();
                }
            }, function(innerErr) {
                callback(innerErr, result);
            });

        }
    );
};

const getDashboardMonthlyOverallStatus = function(req, callback) {
    let selection = {...req.selection};

    if(req.selection['node.location_flat']) {
        if(req.query.parent_id) {
            selection["$and"] = [ 
                {'node.location_flat': req.query.parent_id},
                {'node.location_flat': req.selection['node.location_flat']}
            ];
        } else {
            selection['node.location_flat'] = req.selection['node.location_flat'];
        }
    } else if(req.selection['node._id']) {
        if(req.query.parent_id) {
            selection['node.location_flat'] = req.query.parent_id;
        }
        selection['node._id'] = req.selection['node._id'];
    }


    selection = [
        {
            '$match': selection
        },
        {
            '$group': {
                _id: "$date",
                blue: {
                    $sum: "$blue"
                    },
                green: {
                    $sum: "$green"
                    },
                yellow: {
                    $sum: "$yellow"
                    },
                red: {
                    $sum: "$red"
                    },
                black: {
                    $sum: "$black"
                    },
                blue_price: {
                    $sum: "$blue_price"
                    },
                green_price: {
                    $sum: "$green_price"
                    },
                yellow_price: {
                    $sum: "$yellow_price"
                    },
                red_price: {
                    $sum: "$red_price"
                    },
                black_price: {
                    $sum: "$black_price"
                    }      
                }
        },
        { $sort : { date : 1 } }
    ];

    DailyNodeLocation.aggregate(selection) 
    .exec(
            function(err, result) {
                // console.log("err3", err);
                // console.log("result13", result);
                callback(err, result);
        
        }
    );
};

// Search nodes and skus by partion name
const searchNodesAndSKUs = function(req, res) {
    let selection = {};
    if(req.selection['node.location_flat']) {
        selection['location_flat'] = req.selection['node.location_flat'];
    } else if(req.selection['node._id']) {
        selection['_id'] = req.selection['node._id'];
    }
    async.parallel([
        function(callback){
            let nodeSelection = {...selection};
            nodeSelection.name = {$regex: req.params.query, $options: 'i'};
            Node.find(nodeSelection, function(err,result) {
                let finalResult = []
                //finalResult = result
                for(let i = 0 ;i<result.length;i++)
                {
                    finalResult.push({_id:result[i]._id,name:result[i].name.concat(" (Node)"),
                    department:result[i].department,location:result[i].location,
                    black:result[i].black,black_price:result[i].black_price,
                    red:result[i].red,red_price:result[i].red_price,
                    yellow:result[i].yellow,yellow_price:result[i].yellow_price,
                    green:result[i].green,green_price:result[i].green_price,
                    blue:result[i].blue,blue_price:result[i].blue_price,
                    clients:result[i].clients,suppliers:result[i].suppliers});
                }
                callback(err, finalResult)

            });
        },
        function(callback) {
            let skuSelection = {...req.selection};
            skuSelection["sku.name"] = {$regex: req.params.query, $options: 'i'};

            NodeSKUMap.aggregate([
                {
                    $match: skuSelection
                },
                {
                    '$group': {
                        _id: "$sku._id",
                        name: {$first: "$sku.name"}
                    }
                }
            ], callback);

            //   SKUMaster.find({name: {$regex: req.params.query}}, function(err,result1){
            //       let finalResult1 = [];
            //       for (let j=0;j<result1.length;j++ ){
            //           finalResult1.push({_id:result1[j]._id,
            //           name:result1[j].name.concat(" (SKU)"),
            //           skucode:result1[j].skucode,
            //           comment_count:result1[j].comment_count,
            //           meta:result1[j].meta})
            //       }
            // callback(err,finalResult1)
            // });
            // callback(null, []);
        },

    ],function(err,result){
        //console.log("result is ++++",result)
        let merged = [].concat.apply([], result);
        // for (let i=0;i<result.length;i++){
        //     for (let j=0;j<result[i].length;j++)
        //     {
        //         console.log(result[i])
        //     }
        // };
        apiResponse.sendResponse(merged,200,res) })

   }; 

const getBestPerformingNodes = function(req, callback) {

};




// GET /api/me/orders
// router.get('/me/orders', auth.loggedInUserAuthorization, getAllUserOrders);
// router.get('/me/receipts', auth.loggedInUserAuthorization, myReceipts);

router.get('/users', api.users);
router.post('/user', api.addUser);
router.post('/user/signup', userSignup);
router.get('/user/me', auth.authorizationCheck, api.getprofile)
router.get('/user/me/dashboard', auth.loggedInUserAuthorization, getDashboardInfo)
router.get('/user/me/stocknorm', auth.loggedInUserAuthorization, getUserStocknorm);
router.get('/user/me/locations', auth.loggedInUserAuthorization, getUserAccessibleLocation);
router.get('/user/me/nodesbychange', auth.loggedInUserAuthorization, getNodeSku);
router.get('/user/me/search/:query', auth.loggedInUserAuthorization, searchNodesAndSKUs);
router.get('/user/me/suppliertrend/:id', auth.loggedInUserAuthorization, getSupplierTrend);
router.get('/user/me/skutrend/:id', auth.loggedInUserAuthorization, getSKUTrend);
router.get('/user/me/consumertrend/:id', auth.loggedInUserAuthorization, getConsumerTrend);
router.route('/user/:id')
    .get(api.user)
    .put(api.editUser)
    .delete(api.deleteUser);


module.exports = router;

//{"node._id": {$in: [ObjectId("5f7760d635f1caf010a1d6f5"), ObjectId("5f7760a535f1caf010a1d6f3"), ObjectId("5f7760ec35f1caf010a1d6f6")}
