// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    SKUMeta = mongoose.models.SKUMeta,
    //User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

// mongoose.set('debug', true);

// ALL
const SKUMetas = function(req, res) {
    SKUMeta.find(apiResponse.getStandardDBResponseHandler(res, true));
};

// GET 
// Retrive single laundry category information
const getSKUMeta = function(req, res) {
    SKUMeta.findById(req.params.id,
        apiResponse.getStandardDBResponseHandler(res, true));
};

// POST
// Add a laundry category information
const addSKUMeta =  function(req, res) {
    let SKUMetas = new SKUMeta(req.body);
   SKUMeta.findById(req.body.skumetaparent_id,(err,result)=>{
        SKUMetas.parent = result;
        SKUMetas.save(apiResponse.getStandardDBResponseHandler(res, true));
         })
         
    };

// PUT
// Edit new laundry category information
const editSKUMeta = function(req, res) {
    var id = req.params.id;
    SKUMeta.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
};


// DELETE
// Delete a laundry category information
const deleteSKUMeta = function(req, res) {
    var id = req.params.id;
    return SKUMeta.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));

};

// GET /api/skumeta
// To get all categories from laundry-category resource 
router.get('/skumetas', SKUMetas);

// POST /api/
// To add a category in laundry-category resource
router.post('/skumeta', addSKUMeta);

// GET,PUT And DELETE /api/skumeta/:id
// To get,update and delete a category from laundry-resource by using _id
router.route('/skumeta/:id')
    .get(getSKUMeta)
    .put(editSKUMeta)
    .delete(deleteSKUMeta);



module.exports = router;