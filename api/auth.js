// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    User = mongoose.models.User,
    jwt = require('jwt-simple'),
    moment = require('moment'),
    async = require('async'),
    uuid = require('uuid');
api = {};

var apiAuth = require('../utils/apiAuth');
var apiResponse = require('../utils/apiResponse');
var apiErrors = require('../utils/apiErrors');
var config = require('../config/config');
var validatorAuth = require('../utils/validators/validatorAuth');
var response = null;

/*=============================================>>>>>
= API processing methods =
===============================================>>>>>*/

/**
 * Register user
 */
api.localSignUp = function(req, res) {
    async.waterfall([
        function(callback) {
            User.findOne({ "email": req.body.email }, callback);
        },
        function(user, callback) {
            if (null == user) {
                var user = new User();
                user.email = req.body.email;
                user.password = req.body.password;
                user.name = req.body.name;
                user.is_logged_in = true;
                user.last_login = null;
                user.is_verified = false;
                user.role = config.USER_ROLE.USER;

                user.save(user, callback);
            } else {
                return apiResponse.sendError(apiErrors.USER.EMAIL_EXISTS, null, 200, res);
            }
        }
    ], function(err, result) {
        if (err || result == null) {
            return apiResponse.sendError(apiErrors.APPLICATION.INTERNAL_ERROR, null, 500, res);
        } else {
            var data = { user: result.getMinorDetails };
            data.token = createJWT(result.getMinorDetails, config.USER_ROLE.USER);
            apiResponse.sendResponse(data, 200, res);
        }
    });
};

/**
 * User login
 */
api.localLogin = function(req, res) {
    console.log(req.body.password)
    async.waterfall([
        function(callback) {
            User.findOne({ "email": req.body.email },
                function(err, user) {
                    if (err) {
                        return apiResponse.sendError(apiErrors.USER.LOGIN_FAILED, null, 200, res);
                    }
                    callback(err, user);
                });
        },
        function(user, callback) {
            if (null !== user) {
                user.comparePassword(req.body.password, function(err, isMatch) {
                    callback(err, user, isMatch);
                });
            } else {
                return apiResponse.sendError(apiErrors.USER.LOGIN_FAILED, null, 200, res);
            }
        },
        function(user, passwordVerified, callback) {
            if (passwordVerified) {
                user.last_login = new Date();
                user.is_logged_in = true;
                user.save(callback);
            } else {
                return apiResponse.sendError(apiErrors.USER.LOGIN_FAILED, null, 200, res);
            }
        }
    ], function(err, result) {
        if (!err && result) {
            var data = { user: result.getMinorDetails };

            data.token = createJWT(result.getMinorDetails, result.role);
            req.user_id = result._id;
            req.headers[config.X_AUTHORIZATION_HEADER] = data.token;

            if(result.role === "super_admin") {

              apiResponse.sendResponse(data, 200, res);
            } else {
              if(result.access.nodes) {

              }
              else if(result.access.locations) {
                  
              }

            }
        }
    });
};

/**
 * Admin signup
 * ## DELETE METHOD ONCE SETUP DB ##
 */
api.adminSignUp = function(req, res) {

    User.findOne({
        "email": req.body.email
    }, function(err, existingUser) {
        if (existingUser) {
            // res.status(409).send({ message: 'Email is already taken' });
            response = apiResponse.sendResponse([{
                message: "Email is already taken"
            }], 200, res);
            // res.status(200).send(response);
        } else {

            var user = new User();

            user.email = req.body.email;
            user.password = req.body.password;
            console.log(user.password)
            user.name = req.body.name;
            user.gender = req.body.gender;
            user.phone_no = req.body.mobileNo;
            user.city = req.body.city;
            user.is_logged_in = true;
            user.last_login = new Date().toISOString(),
                user.register_time = new Date().toISOString(),
                user.is_email_verified = false;

            user.role = config.USER_ROLE.SUPER_ADMIN;

            // if (req.body.role === config.USER_ROLE.LAUNDRY || req.body.role === config.USER_ROLE.RESTAURANT) {
            //     user.role = req.body.role;
            // } else {
            //     user.role = config.USER_ROLE.ADMIN;
            // }

            user.save(function(err, result) {
                console.log(err);
                if (err) {
                    // res.status(500).send({
                    //     message: err.message
                    // });

                    console.log(err);
                    return;
                }


                return apiResponse.sendResponse([{
                    token: createJWT(user, config.USER_ROLE.SUPER_ADMIN),
                    user: user,
                    status: 200
                }], true);
            });
        }
    });
};

/**
 * Admin login
 */
api.adminLogin = function(req, res) {
    console.log(req.body.password);
    async.waterfall([
        function(callback) {
            User.findOne({
                "email": req.body.email,
                role: { $ne: config.USER_ROLE.USER }
            }, callback);
        },
        function(user, callback) {
            if (user !== null) {
                user.comparePassword(req.body.password, function(err, isMatch) {
                    if (err) {
                        console.log(err);
                    }
                    callback(err, user, isMatch);
                });

            } else {
                console.log("User not found !");
            }
        },
        function(user, passwordVerified, callback) {
            if (passwordVerified) {
                user.last_login = new Date();
                user.is_logged_in = true;
                user.save(callback);
            } else {
                return apiResponse.sendError(apiErrors.USER.LOGIN_FAILED, null, 200, res);
            }
        }
    ], function(err, result) {
        var data = { user: result.getMinorDetails };
        data.token = createJWT(result.getMinorDetails, result.role);
        data.user = result.getMinorDetails;
        console.log("sending response ", result);
        apiResponse.sendResponse(data, 200, res);

    });
};

/*= End of API processing methods =*/
/*=============================================<<<<<*/


/*=============================================>>>>>
= Private methods Section =
===============================================>>>>>*/

function createJWT(user, userRole) {
    var payload = {
        sub: { id: user._id, role: userRole, firm: user.firm },
        iat: moment().unix(),
        exp: moment().add(14, 'days').unix()
    };
    return jwt.encode(payload, config.TOKEN_SECRET);
}

function sendNewRegisterEmail(email, name, verificationCode) {
    var verificationLink = config.WEBSITE_BASE_URL + config.WEBSITE_EMAIL_VERIFICATION_API;
    verificationLink += "?code=" + verificationCode;

    var mailData = {
        username: name,
        verification_code: verificationLink
    }


    mailHelper.sendVerificationMail(mailHelper.categories.REGISTER, email, mailData, function(err, success) {
        if (err) {
            console.log(err);
        } else {
            console.log('mail success : ' + success);
        }
    });
}
/*= End of Private Methods =*/
/*=============================================<<<<<*/

/*=============================================>>>>>
= Routes =
===============================================>>>>>*/

router.post('/auth/signup', validatorAuth.validateLocalSignup, api.localSignUp);
router.post('/auth/login', validatorAuth.validateLocalLogin, api.localLogin);

router.post('/auth/signin', validatorAuth.validateLocalLogin, api.adminLogin);
router.post('/auth/superadmin/signup', api.adminSignUp);

module.exports = router;
