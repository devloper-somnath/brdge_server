// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    StockTrend = mongoose.models.StockTrend,
    ///User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

// mongoose.set('debug', true);

// ALL
const getAllStockTrendData = function(req, res) {
    
    let selection = {}
    if(req.query.skuid!=null) {
        selection['sku.id'] = req.query.skuid; 
    }
    StockTrend.find(selection,apiResponse.getStandardDBResponseHandler(res, true));
};

// GET 
// Retrive single stocktrend data 
const getStockTrend = function(req, res) {
    StockTrend.findById(req.params.id,
        apiResponse.getStandardDBResponseHandler(res, true));
};

// POST
// Add a DailySns Data
const addStockTrend = function(req, res) {
    let stocktrend = new StockTrend(req.body);
    stocktrend.save(apiResponse.getStandardDBResponseHandler(res, true));
};

// PUT
// Edit new Dailysns data
const editStockTrend = function(req, res) {
    var id = req.params.id;
    StockTrend.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
};


// DELETE
// Delete a DailySns data
const deleteStockTrend = function(req, res) {
    var id = req.params.id;
    return StockTrend.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));

};

// GET /api/dailysnsdata
// To get all stocktrend data on daily  
router.get('/stocktrenddata', getAllStockTrendData);

// POST /api/dailysns
// To add daily stocktred data 
router.post('/stocktrend',addStockTrend);

// GET,PUT And DELETE /api/dailysns/:id
// To get,update and delete stocktrend by using _id
router.route('/stocktrend/:id')
    .get(getStockTrend)
    .put(editStockTrend)
    .delete(deleteStockTrend);



module.exports = router;