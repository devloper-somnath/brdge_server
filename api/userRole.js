// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    UserRole = mongoose.models.UserRole,
    
    ///User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

    // mongoose.set('debug', true);


//display all userrole

  const allUserRole = function (req,res) {
    UserRole.find(apiResponse.getStandardDBResponseHandler(res, true))
  }





// GET 
// Retrive single userrole information
  const getUserRole = function(req, res) {
    UserRole.findById(req.params.id,
      apiResponse.getStandardDBResponseHandler(res, true));
  };

// POST
// Add a userrole information

  const addUserRole =   function(req, res) {
      console.log("req.body++++++",req.body)
    let userrole = new UserRole(req.body);
   
      userrole.save(apiResponse.getStandardDBResponseHandler(res, true));
      
  };

// PUT
// Edit userrole information
  const editUserRole = function(req, res) {
    var id = req.params.id;
    UserRole.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
  };


// DELETE
// Delete userrole information
  const deleteUserRole = function(req, res) {
    var id = req.params.id;
    return UserRole.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));
  };


 
//api/all userrole
  router.get('/alluserrole',allUserRole)

// GET /api/userrole
// To get all userrole details 
//   router.get('/userrole', getUserRole);

// POST /api/userrole
// To add a userrole information
  router.post('/userrole', addUserRole);

// GET,PUT And DELETE /api/userrole/:id
// To get,update and delete a userrole by using _id
router.route('/userrole/:id')
    .get(getUserRole)
    .put(editUserRole)
    .delete(deleteUserRole);



module.exports = router;