// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    NodeMap = mongoose.models.NodeMap,
    SKUMaster = mongoose.models.SKUMaster,
    Node = mongoose.models.Node,
    NodeSKUMap = mongoose.models.NodeSKUMap,
    
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

// mongoose.set('debug', true);

// ALL
const getallnodemaps =  function(req, res) {
    let selection = {} ; 

    if( req.query.supplier!=null ) {
                    selection['supplier._id'] = ObjectId(req.query.supplier)
            }
            if(req.query.child!=null) {
                selection['child._id'] = ObjectId(req.query.child)
            }
        NodeMap.find(selection, apiResponse.getStandardDBResponseHandler(res, true));
    
       };

// GET 
// Retrive single laundry category information
const getNodemap =  function(req, res) {
    NodeMap.findById(req.params.id,
        apiResponse.getStandardDBResponseHandler({supplier:supplier,child:child},res, true));
    };
    
    

// POST
// Add a laundry category information
const addnodemap =  function(req, res) {
    console.log("execution start")
    let nodemap = new NodeMap(req.body);
    NodeMap.findOne({"skumaster._id":ObjectId(req.body.skumaster_id),
     "supplier._id":ObjectId(req.body.suppliernode_id),
     "child._id":ObjectId(req.body.childnode_id)}, function (err,result) {
         console.log("result"+result)
            if(result){
                apiResponse.sendError(apiErrors.CONSUMERSUPPLIERNODE.
                    CONSUMERSUPPLIERNODECHECK,null,200,res)
            }
            else {
                console.log("else executed")
                if(req.body.supplier_sku_code!=null && req.body.child_sku_code!=null){
                    nodemap.supplier_sku_code = req.body.supplier_sku_code
                    nodemap.child_sku_code = req.body.child_sku_code
                     } else{
             NodeSKUMap.findById(req.body.skumaster_id,(err,result)=>{
                console.log("skumaster")
                console.log("nodesku result is",result)
                        nodemap.skumaster = result.sku;
                     nodemap.supplier_sku_code = result.skucode;
                    nodemap.child_sku_code = result.skucode
            Node.findById(req.body.suppliernode_id,(err,result)=>{
                console.log("supplier node")
                 nodemap.supplier = result;
            Node.findById(req.body.childnode_id,(err,result)=>{
                console.log("child node")
                 nodemap.child = result;
            nodemap.save(apiResponse.getStandardDBResponseHandler(res, false));
                                 })
                                })  //nodefindby id closed
                            })     // skumaster findby id closed
                        }         //innner else closed
                      }           //else closed
                    
                })              // nodemap findone closed
};


// PUT
// Edit new laundry category information
const editNodemap = function(req, res) {
    var id = req.params.id;
    NodeMap.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
};


// DELETE
// Delete a laundry category information
const deleteNodemap = function(req, res) {
    var id = req.params.id;
    return NodeMap.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));

};

// GET /api/NodeMap
// To get all categories from laundry-category resource 
router.get('/nodemaps', getallnodemaps);

// POST /api/NodeMap
// To add a category in laundry-category resource
router.post('/nodemap', addnodemap);

// GET,PUT And DELETE /api/NodeMap/:id
// To get,update and delete a category from laundry-resource by using _id
router.route('/nodemap/:id')
    .get(getNodemap)
    .put(editNodemap)
    .delete(deleteNodemap);



module.exports = router;