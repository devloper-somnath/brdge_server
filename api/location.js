// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    Location = mongoose.models.Location,
    StockNorm = mongoose.models.StockNorm,
    NodeSKUMap = mongoose.models.NodeSKUMap,
    Node = mongoose.models.Node,
    ///User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

    // mongoose.set('debug', true);


//display all location on client side

  const allLocation = function (req,res) {
    Location.find(apiResponse.getStandardDBResponseHandler(res, true))
  }

// ALL location to display bar chart on dashboard
  const getallLocations = function(req, res) {
    console.log("parentid in location is",req.query.parentid)
    let selection = {};
    if( req.query.parentid!=null ) {
      selection['parent._id'] = ObjectId(req.query.parentid)
    }
    else {
      selection['parent'] = null
    }
    Location.find(selection,function(err,result) {
      if(result.length==0) {
        console.log("location if excuted ")
        let nodeSelection={} 
        // nodeSelection['node.location._id'] = req.query.parentid
        nodeSelection['location._id'] = ObjectId(req.query.parentid);
        Node.find(nodeSelection,apiResponse.getStandardDBResponseHandler(res, true))
      }
      else {
        console.log("location else excuted ")
       apiResponse.sendResponse(result,200,res)
     }
   })//.limit(5)
  };


// get all location where parent is null
// const parentLocation = function (req,res) {
//     Location.find({'parent':null},apiResponse.getStandardDBResponseHandler(res, true))
// }
//{parent:{$exists:false}},


// GET 
// Retrive single location information
  const getLocation = function(req, res) {
    Location.findById(req.params.id,
      apiResponse.getStandardDBResponseHandler(res, true));
  };

// POST
// Add a location information

  const addLocation =   function(req, res) {
    let location = new Location(req.body);
    Location.findById(req.body.parentlocation_id,(err,result)=>{
      console.log("parent Locaation is",result)
      location.parent = result;
      location.save(apiResponse.getStandardDBResponseHandler(res, true));
    })    
  };

// PUT
// Edit location information
  const editLocation = function(req, res) {
    var id = req.params.id;
    Location.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
  };


// DELETE
// Delete location information
  const deleteLocation = function(req, res) {
    var id = req.params.id;
    return Location.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));
  };


//get parent location 
//api/all locations
  router.get('/alllocations',allLocation)

// GET /api/location
// To get all location details 
  router.get('/locations', getallLocations);

// POST /api/location
// To add a location information
  router.post('/location', addLocation);

// GET,PUT And DELETE /api/location/:id
// To get,update and delete a location by using _id
router.route('/location/:id')
    .get(getLocation)
    .put(editLocation)
    .delete(deleteLocation);



module.exports = router;