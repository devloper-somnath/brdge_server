// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,

    async = require("async"),
    multer = require('multer'),
    moment = require('moment'),
    excelToJson = require('convert-excel-to-json'),
    auth = require('../utils/apiAuth'),
     _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');
    require
    StockNorm =  mongoose.models.StockNorm,
    NodeMap = mongoose.models.NodeMap,
    NodeSKUMap = mongoose.models.NodeSKUMap,
    SKUMaster = mongoose.models.SKUMaster,
    Node = mongoose.models.Node,
    SNSTrend = mongoose.models.SNSTrend,
    User = mongoose.models.User,
    Location = mongoose.models.Location,
    DailyNodeLocationAggrigate = mongoose.models.DailyNodeLocation,
    WeeklyNodeLocationAggrigate = mongoose.models.WeeklyNodeLocation,
    MonthlyNodeLocationAggrigate = mongoose.models.MonthlyNodeLocation,
    SkuAvailability = mongoose.models.SkuAvailability,
    BestPerformingNode = mongoose.models.BestPerformingNode,
    BestPerformingSku = mongoose.models.BestPerformingSku;
    // mongoose.set('debug', true);

// multer code for file upload

    var storage = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, 'uploads/');
        },
        filename: function(req, file, cb) {
            // console.log("renaming ", file);
            var name = file.originalname;
            var extension = name.split('.');
            extension = extension[extension.length - 1];
            cb(null, Date.now() + "." + extension);
        }
    });

    var upload = multer({ storage: storage });
// multer  file upload code end


// get node-sku by availibiltyzone to show on dashboard table
    const getNodeSku = function (req,res) {
        let selection ={}
        if(req.query.availability_zone!=null) {
            selection['availability_zone'] = req.query.availability_zone
        }
        NodeSKUMap.find(selection,apiResponse.getStandardDBResponseHandler(res, true)).sort({ days_seen_last_changed: -1 });
    }

// get node-sku by parentid to show dasbboaerd table after click on node name

    const getNodeSkuByParentId = function (req,res) {
        let selection = {} ;
        if( req.query.parentid!=null ) {
            selection['node.location._id'] = req.query.parentid
        }
        NodeSKUMap.find(selection,apiResponse.getStandardDBResponseHandler(res, true));
    }


//  retrive ALL data from nodesku
    const getallnodesku = function(req, res) {
        let selection = {} ;
         console.log("node id is",req.query.id)
        if( req.query.id!=null ) {
            selection['node._id'] = ObjectId(req.query.id)
        }
        NodeSKUMap.find(selection,apiResponse.getStandardDBResponseHandler(res, true));

        // NodeSKUMap.find({"node._id":req.query.id}).lean().exec(function(err, result) {
        //     console.log("\n\n\n----result---",result)
        // });

    };

    //GET
// Search nodes and skus by partion name
const searchNodesAndSKUs = function(req, res) {
    // console.log("req.params.query.++++",req.params.query)
    // async.parallel({
    //   nodes: function(callback) {
    //     Node.find({name: {$regex: req.params.query}}, callback);
    //   },
    //   skus: function(callback) {
    //     SKUMaster.find({name: {$regex: req.params.query}, skucode: {$regex: req.params.query}}, callback);
    //   },

    // },apiResponse.getStandardDBResponseHandler(res, true))



    async.parallel([
        function(callback){
              Node.find({name: {$regex: req.params.query}}, function(err,result){
              let finalResult = []
              //finalResult = result
              for(let i = 0 ;i<result.length;i++)
                {
                    finalResult.push({_id:result[i]._id,name:result[i].name.concat(" (Node)"),
                    department:result[i].department,location:result[i].location,
                    black:result[i].black,black_price:result[i].black_price,
                    red:result[i].red,red_price:result[i].red_price,
                   yellow:result[i].yellow,yellow_price:result[i].yellow_price,
                 green:result[i].green,green_price:result[i].green_price,
                 blue:result[i].blue,blue_price:result[i].blue_price,
                clients:result[i].clients,suppliers:result[i].suppliers});
                }
            callback(err,finalResult)

            });
        },
        function(callback){
              SKUMaster.find({name: {$regex: req.params.query}}, function(err,result1){
                  let finalResult1 = [];
                  for (let j=0;j<result1.length;j++ ){
                      finalResult1.push({_id:result1[j]._id,
                      name:result1[j].name.concat(" (SKU)"),
                      skucode:result1[j].skucode,
                      comment_count:result1[j].comment_count,
                      meta:result1[j].meta})
                  }
            callback(err,finalResult1)
            });
        },

    ],function(err,result){
        //console.log("result is ++++",result)
        let merged = [].concat.apply([], result);
        // for (let i=0;i<result.length;i++){
        //     for (let j=0;j<result[i].length;j++)
        //     {
        //         console.log(result[i])
        //     }
        // };
        apiResponse.sendResponse(merged,200,res) })

   };    //

// GET
// Retrive single value from nodesku
const getnodesku = function(req, res) {
    NodeSKUMap.findById(req.params.id,apiResponse.getStandardDBResponseHandler(res, true));
};

// POST
// Add a nodesku data information through frontend form

const addnodesku =  function(req, res) {    
    let nodeskumap = new NodeSKUMap(req.body);
    // console.log("nodeskumap",nodeskumap)
    NodeSKUMap.findOne({"sku._id":ObjectId(req.body.skumaster_id),
    "node._id":ObjectId(req.body.node_id)}, function (err,result) {

        if(result) {
        return apiResponse.sendError(apiErrors.NODESKU.DUPLICATE_SKUNODE_PAIR,null,200,res)
        }
        else {
        SKUMaster.findById(req.body.skumaster_id,(err,skuresult)=>{
            nodeskumap.sku = skuresult;
            Node.findById(req.body.node_id,(err,noderesult)=>{
                nodeskumap.node = noderesult;

                User.findById(req.body.buyer, {name: 1}, function(err, buyer) {
                    nodeskumap.buyer = buyer;    
                    nodeskumap.save(apiResponse.getStandardDBResponseHandler(res, false));
                });
                
            })
        })
    }
})
};


 //retrive data from xlsx.file and store in node-sku table,
 //dailynodelocationaggrigate,weeklynodelocationaggrigate,
 //monthlynodelocationaggrigate


    const addnodeskufromfile =  function(req, res) {

    let filedata = [];
    let data = {};
    let toJsonResult = [];
    let nodePerformanceRecord = {};
    let skuPerformanceRecord = {};
    let nodeIDs = [];
    let skuIDs = [];
    let snsData ={};

    if (req.files['files']) {
     let files = req.files['files'];
        files.map((file) => {
            data.file = file.path;
                toJsonResult = excelToJson({
                    sourceFile: data.file,
                    header:{
                        rows:1
                    }
                });

            for (let i=0;i<=toJsonResult.Sheet1.length-1;i++) {
                filedata.push(toJsonResult.Sheet1[i])
            }
        })   //// files closed
    }       // if closed


    async.eachSeries(filedata, function(item,callback) {
        

        let availabilityzone=0;
        let days_seen_last_changed = 0;
        let percentage = (item.G/item.F)*100;

        if(percentage==0) {
            availabilityzone = 0
        }
        if(percentage>0 && percentage<=33) {
            availabilityzone = 1
        }
        if(percentage>33 && percentage<=66) {
             availabilityzone = 2
        }
        if(percentage>66 && percentage <= 100) {
             availabilityzone = 3;
        }
        if(percentage > 100) {
            availabilityzone = 4;
        }

        Node.findOne({"name":item.E},function(err,noderesult) {
          if(nodeIDs.indexOf(noderesult._id) == -1) {
            nodeIDs.push(noderesult._id);
          }

            SKUMaster.findOne({"skucode":item.B},function(err,skuresult) {
              if(skuIDs.indexOf(skuresult._id) == -1) {
                skuIDs.push(skuresult._id);
              }

              
              // build SNS table
              populateSNS(item.B,item.E,item.F,item.G,noderesult,availabilityzone,item.H)





                //,"node.name":item.E,"sku.skucode":item.B
                NodeSKUMap.findOne({"availability_zone":availabilityzone,
                                    "node.name":item.E,"sku.skucode":item.B},
                                    function(err,nodeskuresult) {
                                        //nodeskuresult.stock[item.H] = {stockNorm:item.F,actualStock:item.G,AvailabilityZone:availabilityzone};
                                        if(nodeskuresult==null) {
                                            days_seen_last_changed = 1;
                                        }
                                        else {
                                            days_seen_last_changed = nodeskuresult.days_seen_last_changed + 1 ;
                                        }
                                        
                                        // sns data for stocktrend report 
                                        snsData[moment(item.H).format('YYYY-MM-DD')]  = {stockNorm:item.F,actualStock:item.G,AvailabilityZone:availabilityzone}
            NodeSKUMap.update(
            {"sku.skucode":item.B,"node.name":item.E},
            {
                "stock" : snsData,
                "node": noderesult,
                "sku": skuresult,
                "stock_norm" : item.F,
                "actual_stock" : item.G,
                "availability_zone": availabilityzone,
                "days_seen_last_changed": days_seen_last_changed
            },
            function(err,updateResult) {
                if(err) {
                    console.log(err)
                }
                else {

                  // Update performance schema
                  updateNodeSKUPerformance(noderesult, skuresult, item.F - item.G, nodePerformanceRecord);
                  updateSKUPerformance(noderesult, skuresult, item.F - item.G, skuPerformanceRecord);
                 let availabilityzoneCounter = {};

                     if(availabilityzone==0) {
                        availabilityzoneCounter.black = 1,availabilityzoneCounter.black_price =
                        skuresult.price;

                    }
                    if(availabilityzone==1) {
                    availabilityzoneCounter.red = 1,availabilityzoneCounter.red_price =
                    skuresult.price;

                    }
                    if(availabilityzone==2) {
                        availabilityzoneCounter.yellow = 1,availabilityzoneCounter.yellow_price =
                        skuresult.price;

                    }
                    if(availabilityzone==3) {
                    availabilityzoneCounter.green = 1,availabilityzoneCounter.green_price =
                    skuresult.price;

                    }
                    if(availabilityzone==4) {
                        availabilityzoneCounter.blue = 1,availabilityzoneCounter.blue_price =
                        skuresult.price;

                    }

                    let colorCounter = {};
                    if(availabilityzone==0) {
                        colorCounter.black = 1,colorCounter.days_in_black = 1;
                    }
                    if(availabilityzone==1) {
                        colorCounter.red = 1,colorCounter.days_in_red = 1;
                    }
                    if(availabilityzone==2) {
                        colorCounter.yellow = 1,colorCounter.days_in_yellow = 1;
                    }
                    if(availabilityzone==3) {
                        colorCounter.green = 1,colorCounter.days_in_green = 1;
                    }
                    if(availabilityzone==4) {
                        colorCounter.blue = 1,colorCounter.days_in_blue = 1;
                    }
                 //console.log("for node update node name is ",item.E)
                 //console.log("availabilityzoneCounter object ",availabilityzoneCounter)
                    Node.update (
                        {"name":item.E},
                        {$inc:availabilityzoneCounter},
                        function(err,Nodeupdate){
                            if(err) {
                                console.log("err",err)
                            }
                            else {
                               // console.log("nodeupdated ")
                            }
                        }
                    )

                    DailyNodeLocationAggrigate.update(
                        {"node.name":item.E,"date":item.H},
                        {
                            node:noderesult,
                            $inc:availabilityzoneCounter
                        },
                        {upsert:true},
                        function(err,dailyupdate){
                         // console.log("noderesult.location+++++++++++",noderesult.location)

                            let parentLocation = noderesult.location;
                                while(parentLocation!=null) {

                                    SkuAvailability.update({"location.name":parentLocation.name,
                                     "sku.name":skuresult.name},
                                    {
                                        "sku":skuresult,
                                        "location":parentLocation,
                                        $inc:colorCounter
                                    },
                                    {upsert:true},function(err,updateResult) {
                                    console.log("skuavailability updated")
                                })
                                    Location.update(
                                        {"name":parentLocation.name},
                                        {$inc:availabilityzoneCounter},function(err,locationupdate){
                                            console.log("location updated")
                                        })

                                    DailyNodeLocationAggrigate.update(
                                        {"node.name":parentLocation.name,"date":item.H},
                                        {
                                            node:parentLocation,
                                            date:item.H,
                                            $inc:availabilityzoneCounter
                                        },{upsert:true},function(err,parentlocationupdate) {
                                           console.log("parent updated")
                                        })   //while loop update closed
                                        parentLocation = parentLocation.parent
                                }            //while loop closed


                                let init = moment(item.H);
                                let dateofweek = init.startOf('week').toDate();

                                WeeklyNodeLocationAggrigate.update(
                                    {"node.name":item.E,"date":dateofweek},
                                    {

                                        node:noderesult,
                                        $inc:availabilityzoneCounter
                                    },
                                    {upsert:true},
                                    function(err,weeklyupdate){
                                    if(err){
                                       console.log(err)
                                    }
                                    else{

                                let parentLocation = noderesult.location;
                                while(parentLocation!=null) {
                                    WeeklyNodeLocationAggrigate.update(
                                        {"node.name":parentLocation.name,"date":dateofweek},
                                        {
                                            node:parentLocation,
                                            date:dateofweek,
                                            $inc:availabilityzoneCounter
                                        },{upsert:true},function(err,parentlocationupdate) {
                                            //console.log("parentLocation updated")

                                        })   //while loop update closed
                                        parentLocation = parentLocation.parent
                                }            //while loop closed
                                        let init = moment(item.H);
                                        let dateofmonth = init.startOf('month').toDate();
                                        MonthlyNodeLocationAggrigate.update(
                                        {"node.name":item.E,"date":dateofmonth},
                                        {
                                            node:noderesult,
                                            $inc:availabilityzoneCounter
                                        },
                                        {upsert:true},
                                        function(err,monthlyupdate){
                                            if(err){
                                                console.log(err)
                                            }
                                            else{
                                let parentLocation = noderesult.location;
                                while(parentLocation!=null) {
                                    MonthlyNodeLocationAggrigate.update(
                                        {"node.name":parentLocation.name,"date":dateofmonth},
                                        {
                                            node:parentLocation,
                                            date:dateofmonth,
                                            $inc:availabilityzoneCounter
                                        },{upsert:true},function(err,parentlocationupdate) {
                                            //console.log("parentLocation updated")

                                        })   //while loop update closed
                                        parentLocation = parentLocation.parent
                                }            //while loop closed
                                                //console.log("monthly data updated")
                                            }
                                        }
                                        )
                                    }
                                }
                                )   //weekly update closed
                                //}   //dailyupdate callback else closed
                               // callback();
                         })        //dailyupdate closed
                        callback();

                       }         //else of nodeskumap sucess closed
                     })          //nodeskumap update closed.
                  })             //nodesku findone closed
               // })               //NodeMap find closed.
                })               //sku findone closed
        })                       //node  findoneclosed

        },function(err, result) {
          console.log("++outer callback+++++")
          if(err){
            console.log("in error");
              console.log(err)
           }
          else {
            console.log("in success");
              let nodeEntries = [];
              let skuEntries = [];
              for(let index = 0; index < nodeIDs.length; ++index) {
                if(nodePerformanceRecord[nodeIDs[index]]) {
                  nodeEntries.push(nodePerformanceRecord[nodeIDs[index]]);
                }
              }
              BestPerformingNode.create(nodeEntries, function(err) {
                //apiResponse.sendResponse(result,200,res)
                for(let index = 0; index < skuIDs.length; ++index) {
                    if(skuPerformanceRecord[skuIDs[index]]) {
                        skuEntries.push(skuPerformanceRecord[skuIDs[index]]);
                    }
                  }
                  BestPerformingSku.create(skuEntries, function(err) {
                    apiResponse.sendResponse(result,200,res)
                  });
              });



          }

     })   //async each closed

};


// PUT
// Edit new nodesku information
const editnodesku = function(req, res) {
    var id = req.params.id;
    NodeSKUMap.findByIdAndUpdate(id, { $set: req.body },apiResponse.getStandardDBResponseHandler(res, false));
};


// DELETE
// Delete a nodesku information
const deletenodesku = function(req, res) {
    var id = req.params.id;
    return NodeSKUMap.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));
};

/*=============================================>>>>>
= Section comment block =
===============================================>>>>>*/

const updateNodeSKUPerformance = function(node, sku, quantity, nodeRecords) {
  const currentSKUPrice = quantity * sku.price;
  //if record already exists in list
  if(nodeRecords[node._id]) {
    let currentRecord = nodeRecords[node._id];

    currentRecord.total += quantity;
    currentRecord.total_price += currentSKUPrice;

    for(let index = 0; index < currentRecord.skus_by_quantity.length; ++index) {
      if(currentRecord.skus_by_quantity[index].quantity <= quantity) {
        currentRecord.skus_by_quantity.splice(index, 0, {sku: sku, quantity: quantity});
        break;
      }
    }

    for(let index = 0; index < currentRecord.skus_by_price.length; ++index) {
      if(currentRecord.skus_by_price[index].quantity <= currentSKUPrice) {
        currentRecord.skus_by_price.splice(index, 0, {sku: sku, quantity: currentSKUPrice});
        break;
      }
    }


  } else { // If node came first time
    let today = moment().startOf('day').toDate();
    let newRecord = {
      node: node,
      skus_by_quantity: [{sku: sku, quantity: quantity}],
      skus_by_price: [{sku: sku, quantity: currentSKUPrice }],
      total: quantity,
      total_price: currentSKUPrice,
      date: today
    };
    nodeRecords[node._id] = newRecord;
  }

};

const updateSKUPerformance = function(node, sku, quantity, skuRecords) {
  const currentSKUPrice = quantity * sku.price;

  if(skuRecords[sku._id]) {
    currentRecord = skuRecords[sku._id];

    currentRecord.total += quantity;
    currentRecord.total_price += currentSKUPrice;

    for(let index = 0; index < currentRecord.nodes_by_quantity.length; ++index) {
      if(currentRecord.nodes_by_quantity[index].quantity <= quantity) {
        currentRecord.nodes_by_quantity.splice(index, 0, {node: node, quantity: quantity});
        break;
      }
    }

    for(let index = 0; index < currentRecord.nodes_by_price.length; ++index) {
      if(currentRecord.nodes_by_price[index].quantity <= currentSKUPrice) {
        currentRecord.nodes_by_price.splice(index, 0, {node: node, quantity: currentSKUPrice});
        break;
      }
    }

  } else {
    let today = moment().startOf('day').toDate();
    let newRecord = {
      sku: sku,
      nodes_by_quantity: [{node: node, quantity: quantity}],
      nodes_by_price: [{node: node, quantity: currentSKUPrice }],
      total: quantity,
      total_price: currentSKUPrice,
      date: today
    };
    skuRecords[sku._id] = newRecord;
  }
};



const populateSNS = function(skucode,nodename,stocknorm,actualstock,noderesult,Availability_Zone,date) {
    let SNSStock = {};
    const dateKey = moment(date).format('YYYY-MM-DD');
    //console.log("DAte is ++++",date)
    SNSStock[dateKey]  = {stockNorm: stocknorm, actualStock:actualstock, AvailabilityZone:Availability_Zone}
    // console.log("sns stock++++",SNSStock)
    NodeMap.find({"skumaster.skucode":skucode,"child.name":nodename},function(err,nodemapresult) {
    //

        let StockNormPer = 0 , ActulStockPer = 0 ;

       for (let i = 0; i < nodemapresult.length; i++) {
        
        StockNormPer = stocknorm*(nodemapresult[i].supply_quantity/100) ;
        ActulStockPer = actualstock*(nodemapresult[i].supply_quantity/100);
        console.log("+++++++++++ split calculations stocknorm " + stocknorm + "  supply " + nodemapresult[i].supply_quantity + "  stocknorm " + StockNormPer + "   Actual stock " + ActulStockPer);
        if(!nodemapresult[i].stock) {
            nodemapresult[i].stock = {};
        } 
        nodemapresult[i].stock[dateKey]  = {stockNorm: StockNormPer, actualStock:ActulStockPer, AvailabilityZone:Availability_Zone};
        nodemapresult[i].save(function(err, document) {
            console.log("\n\n\n @@@@@@@@@@@@@ stock updated");
        });
        //console.log("stocknormper,actualstockper++++++",StockNormPer,ActulStockPer)
        console.log("sns stock++++",SNSStock)
        let snsdata = new StockNorm();
        snsdata.stock = SNSStock;
        snsdata.stock_norm = StockNormPer ;
        snsdata.consumer = noderesult;
        snsdata.supplier =  nodemapresult[i].supplier;
        snsdata.skumaster =  nodemapresult[i].skumaster;
        snsdata.actual_stock = ActulStockPer;
        snsdata.supplier_sku_code = skucode ;
        snsdata.consumer_sku_code  = skucode;
        snsdata.availability_zone = Availability_Zone;

        //console.log("snsdata is ++++",snsdata)

        snsdata.save(function(err,snsdataresult){
            console.log("snsdata saved to sns table++")
        });

        let newTrend = new SNSTrend();
        newTrend.stocknorm = StockNormPer ;
        newTrend.consumer = noderesult;
        newTrend.supplier =  nodemapresult[i].supplier;
        newTrend.sku =  nodemapresult[i].skumaster;
        newTrend.actual_stock = ActulStockPer;
        newTrend.date = date;
        newTrend.availability_zone = Availability_Zone;
        newTrend.save(function(err, savedTRend) {
            console.log("Trend saved");
        });
       }
         // if(nodemapresult.length!=0){
        //       perct =(itemF*nodemapresult.supply_quantity/100) ;
        //       actulstock = (itemG * nodemapresult.supply_quantity/100)
        //   }


        })
}

/*= End of Section comment block =*/
/*=============================================<<<<<*/

// GET /api/NodeSkuMap

// get nodesku by parentid to show dashboared table afetr click on nodename of bar chart
    router.get('/nodeskubyparentid',getNodeSkuByParentId);

// get nodesku by availaibility zone to show on dashboaed tabel
    router.get('/nodeskubyzone',getNodeSku);


// To get all categories from laundry-category resource
   router.get('/nodeskus', getallnodesku);

   // Search nodes and SKUs
  router.get('/nodeskus/search/:query', searchNodesAndSKUs);

// POST /api/NodeSkuMap
// To add a node-sku to databass
   router.post('/nodesku',addnodesku);


// to add node-sku to database from xlsx.file
var cpUpload =  upload.fields([{ name: 'files' }])
router.post('/nodeskufromfile',cpUpload,addnodeskufromfile)



// GET,PUT And DELETE /api/NodeSkuMap/:id
// To get,update and delete a nodesku data  by using _id
router.route('/nodesku/:id')
    .get(getnodesku)
    .put(editnodesku)
    .delete(deletenodesku);



module.exports = router;
