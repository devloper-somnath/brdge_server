const { json } = require('body-parser');
//const stocknorm = require('../models/stocknorm');


// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    StockNorm = mongoose.models.StockNorm,
    SKUMaster = mongoose.models.SKUMaster,
    Node = mongoose.models.Node,
    NodeSKUMap = mongoose.models.NodeSKUMap,
    multer = require('multer'),
    excelToJson = require('convert-excel-to-json'),
    
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

// mongoose.set('debug', true);


var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/');
    },
    filename: function(req, file, cb) {
        console.log("renaming ", file);
        var name = file.originalname;
        var extension = name.split('.');
        extension = extension[extension.length - 1];
        cb(null, Date.now() + "." + extension);
    }
});

var upload = multer({ storage: storage });


// ALL
const getallstocknorm =  function(req, res) {
    let selection = {};
    if(req.query.availability_zone!=null ) {
        selection['availability_zone'] = req.query.availability_zone
       }
    //console.log("selection object",selection)
    StockNorm.find(selection,apiResponse.getStandardDBResponseHandler(res, true));
    
};



// get  stock norm data for particular node (data for redirection of 
// dashboard first table node click)


const getNodeStockNorm =  function(req, res) {
    console.log("node id is ",req.query.nodeid)
    let selection = {};
    if(req.query.nodeid!=null ) {
        selection['consumer._id'] = ObjectId(req.query.nodeid)
       }
    
    StockNorm.find(selection,apiResponse.getStandardDBResponseHandler(res, true));
    
};




//fetch NodeWise data where  city location id found
// const locationWiseNodeData = function (req,res) {
//         let selection = {} ;
//         if(req.query.cityid!=null) {
//           selection['consumer.location._id'] = ObjectId(req.query.cityid)
//         }
//         StockNorm.find(selection,apiResponse.getStandardDBResponseHandler(res, true));
// }


// get summary of stocknorms
// try1


//  const getStockNormSummary = function (req,res) {
//      StockNorm.find({}, function(err,result) {
//           let summary={};
//           let color;
//           let consumer;
//           let summaryList = [];
//           let consumersArray =[];
        
//           if(result) {

//             for (let i=0;i<result.length;i++ ) {
//                let percentage;
//                percentage =(result[i].actual_quantity/result[i].stock_norm)*100;
//                consumer = result[i].consumer;
           
//                 if(percentage==0) {
//                     color =" black"
//                 }
//                 if(percentage>0 && percentage<=33){
//                     color="red"
//                 }
//                 if(percentage>33 && percentage<=66){
//                     color="yellow"
//                 }
//                 if(percentage>66 && percentage <= 100){
//                  color="green"
//                 }
                    
//                 //console.log("Checking consumer", consumer.id);
//                 if(consumersArray.indexOf(consumer.id) == -1) {
//                     //console.log("adding entry consumer", consumer.id);
//                     consumersArray.push(consumer.id);
//                     //console.log(consumersArray)
//                     summary[consumer.id] = {
//                         node: consumer,
//                         black: color === "black" ? 1 : 0,
//                         red: color === "red" ? 1 : 0,
//                         yellow: color === "yellow" ? 1 : 0,
//                         green: color === "green" ? 1 : 0
//                     }
//                     console.log("summary object",summary)
//                 } else {
//                     //console.log("\n\nupdating consumer", consumer.id);
//                    // console.log("color ", color);
//                     //console.log("before update ", summary[consumer.id][color]);
//                     summary[consumer.id][color]++;
//                     //console.log("after update ", summary[consumer.id][color]);
//                    }
// } // result for loop closed

//          console.log("consumers in result", consumersArray);
//          for(let consumerIndex = 0; consumerIndex < consumersArray.length; ++consumerIndex) {
//             summaryList.push(summary[consumersArray[consumerIndex]]);
//              }
//             // console.log(summaryList);
//                 //res.send(summaryList);
                 
//                 apiResponse.sendResponse(summaryList, 200,res)
//             }    //////   result .....if closed
//         })
// };



//new summary data according to availability zone

const getStockNormSummary = function (req,res) {
    StockNorm.find({}, function(err,result) {
         let summary={};
         let color;
         let availability_zone ;
         let consumer;
         let summaryList = [];
         let consumersArray =[];
       
         if(result) {

           for (let i=0;i<result.length;i++ ) {
            availability_zone = result[i].availability_zone; 
              consumer = result[i].consumer;
          
               if(availability_zone==0) {
                   color =" black"
               }
               if(availability_zone==1){
                   color="red"
               }
               if(availability_zone==2){
                   color="yellow"
               }
               if(availability_zone==3){
                color="green"
               }
                   
               //console.log("Checking consumer", consumer.id);
               if(consumersArray.indexOf(consumer.id) == -1) {
                   //console.log("adding entry consumer", consumer.id);
                   consumersArray.push(consumer.id);
                   //console.log(consumersArray)
                   summary[consumer.id] = {
                       node: consumer,
                       black: color === "black" ? 1 : 0,
                       red: color === "red" ? 1 : 0,
                       yellow: color === "yellow" ? 1 : 0,
                       green: color === "green" ? 1 : 0
                   }
                   console.log("summary object",summary)
               } else {
                   //console.log("\n\nupdating consumer", consumer.id);
                  // console.log("color ", color);
                   //console.log("before update ", summary[consumer.id][color]);
                   summary[consumer.id][color]++;
                   //console.log("after update ", summary[consumer.id][color]);
                  }
} // result for loop closed

        console.log("consumers in result", consumersArray);
        for(let consumerIndex = 0; consumerIndex < consumersArray.length; ++consumerIndex) {
           summaryList.push(summary[consumersArray[consumerIndex]]);
            }
           // console.log(summaryList);
               //res.send(summaryList);
                
               apiResponse.sendResponse(summaryList, 200,res)
           }    //////   result .....if closed
       })
};



/// line chart data 

const lineChartData = function (req,res) {

     let dataArray = [
            {
                "label":"day",
                "dayname":"sun",
                "black" : 0,
                "red" :2,
                "yellow" : 1,
                "green":3,
                "blue":3
            },
            {
                "label":"day",
                "dayname":"mon",
                "black" : 2 ,
                "red" :1,
                "yellow" : 3,
                "green":0,
                "blue":2
            },
            {
                "label":"day",
                "dayname":"tue",
                "black" : 2 ,
                "red" :1,
                "yellow" : 2,
                "green":1
            },
            {
                "label":"day",
                "dayname":"wes",
                "black" : 0 ,
                "red" :2,
                "yellow" : 1,
                "green":3,
                "blue":0
            },
            {
                "label":"day",
                "dayname":"thu",
                "black" : 1 ,
                "red" :1,
                "yellow" : 1,
                "green":5,
                "blue":1
            },
            {
                "label":"day",
                "dayname":"fri",
                "black" : 2 ,
                "red" :2,
                "yellow" : 1,
                "green":4,
                "blue":2
            },
            {
                "label" :"day",
                "dayname" :"sat",
                "black" : 1 ,
                "red" :0,
                "yellow" : 3,
                "green":1,
                "blue":1
            },
            {
                "label" :"week",
                "weekname" :"firstweek",
                "black" :2,
                "red" :1,
                "yellow" :2,
                "green" : 3,
                "blue":3
            },
            {
                "label" :"week",
                "weekname" :"secondweek",
                "black" :1,
                "red" :3,
                "yellow" :1,
                "green" : 3,
                "blue":4
            },
            {
                "label" :"week",
                "weekname" :"thirdweek",
                "black" :3,
                "red" :4,
                "yellow" :1,
                "green" : 0,
                "blue":1
            },
            {
                "label" :"week",
                "weekname" :"fourthweek",
                "black" :1,
                "red" :3,
                "yellow" :5,
                "green" : 2,
                "blue":4
            },
            {
                "label" :"week",
                "weekname" :"fifthweek",
                "black" :1,
                "red" :2,
                "yellow" :4,
                "green" : 2,
                "blue":2
            },
            {
                "label" :"week",
                "weekname" :"sixthweek",
                "black" :1,
                "red" :1,
                "yellow" :4,
                "green" : 0,
                "blue":4
            },
            {
                "label" :"week",
                "weekname" :"seventhweek",
                "black" :4,
                "red" :3,
                "yellow" :5,
                "green" : 3,
                "blue":2
            },     {
                "label":"month",
                "monthname" :"jan",
                "black" :4,
               "red" :3,
               "yellow" :5,
               "green" : 3,
               "blue":1
            },
            {
               "label":"month",
               "monthname" :"feb",
               "black" :2,
              "red" :3,
              "yellow" :4,
              "green" : 2,
              "blue":1
           },
           {
               "label":"month",
               "monthname" :"mar",
               "black" :2,
              "red" :1,
              "yellow" :3,
              "green" : 4,
              "blue":4
           },
           {
               "label":"month",
               "monthname" :"april",
               "black" :2,
              "red" :4,
              "yellow" :5,
              "green" : 4,
              "blue":2
           },
           {
               "label":"month",
               "monthname" :"may",
               "black" :1,
              "red" :2,
              "yellow" :3,
              "green" : 4,
              "blue":4
           },
           {
               "label":"month",
               "monthname" :"jun",
               "black" :4,
              "red" :2,
              "yellow" :4,
              "green" : 1,
              "blue":2
           }
    ]; 
 let resultArray =[];

        
         for(let i=0;i<dataArray.length;i++){
             if(dataArray[i].label == req.query.frequency) {
                 resultArray.push(dataArray[i])
                   }

         }  //  for loop closed

         apiResponse.sendResponse(resultArray, 200,res)
        
        
    };// end of main function









// GET 
// Retrive single laundry category information
  const getStockNorm =  function(req, res) {
    StockNorm.findById(req.params.id,
        apiResponse.getStandardDBResponseHandler({supplier:supplier,child:child},res, true));
    };
    
    
    // POST

const addStockNorm =  function(req, res) { 
    // let stocknorm;

    // let data = {};
    // let toJsonResult = [];
    // if (req.files['files']) {
    //     let files = req.files['files'];
    //     console.log('\n\n---my files---', files);
    //       files.map((file) => {
    //             data.file = file.path;
    //             console.log('\n\n---before data file----', data.file)
    //             toJsonResult = excelToJson(
    //                 {sourceFile: data.file}
    //             );

    //             console.log('\n\n---json data---', 
    //             toJsonResult)    
    //         });
    // }

    // for(let i=1;i< toJsonResult.Sheet1.length;i++) { 
    //     stocknorm = new StockNorm({
    //         category : toJsonResult[i].A,
    //         email: toJsonResult[i].B,
    //         password: toJsonResult[i].C,
    //     });
    // }


    let stocknorm = new StockNorm(req.body);
    stocknorm.save(apiResponse.getStandardDBResponseHandler(res, true));
    
    
}

// PUT
// Edit new laundry category information
const editStockNorm = function(req, res) {
    var id = req.params.id;
    StockNorm.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
};


// DELETE
// Delete a laundry category information
const deleteStockNorm = function(req, res) {
    var id = req.params.id;
    return StockNorm.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));

};


// GET /api/stocknorm/location
// router.get('/stocknorm/location',locationWiseNodeData)

// GET /api/stock norms
// To get all categories from laundry-category resource 
router.get('/stocknorms', getallstocknorm);

// GET /api/stocknorm/summary
//to  get all details of stocknorms

router.get('/stocknorm/summary',getStockNormSummary)


// get nodestocknorm for particluar node
router.get('/nodestocknorm',getNodeStockNorm)




//Get /api/linechartdata
//get linechart data according to day or week ,month

router.get('/linechartdata',lineChartData)

// POST /api/stocknorm
// To add a category in laundry-category resource
var cpUpload =  upload.fields([{ name: 'files' }])

// router.post('/stocknorm', cpUpload,addStockNorm);
router.post('/stocknorm',addStockNorm);

// GET,PUT And DELETE /api/stocknorm/:id
// To get,update and delete a category from laundry-resource by using _id
router.route('/stocknorm/:id')
    .get(getStockNorm)
    .put(editStockNorm)
    .delete(deleteStockNorm);



module.exports = router;