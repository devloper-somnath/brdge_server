// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    SkuAvailability = mongoose.models.SkuAvailability,
    ///User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

    // mongoose.set('debug', true);

// get  colorcountdata for first tab in second table of dashoboard
    const getColorCounter= function(req, res) {
        
        let selection ={}
        selection [req.query.color] = -1
 
        // if(req.query.parentid!=null) {
        //     selection['location.parent._id'] = ObjectId(req.query.parentid)
        // }
        // else {
        //     selection["location.name"] = "India"
        // }
        
       SkuAvailability.find({"location.name":"India"},apiResponse.getStandardDBResponseHandler(res, true)).sort(selection)
    };

    const getDaysInColorCount =function(req, res) {
        
        let selection ={}
        selection [req.query.daysincolor] = -1
        SkuAvailability.find({"location.name":"India"},apiResponse.getStandardDBResponseHandler(res, true)).sort(selection)
    };

// GET 
// Retrive single skuavailability information
    const getskuavailability = function(req, res) {
        SkuAvailability.findById(req.params.id,
        apiResponse.getStandardDBResponseHandler(res, true));
    };

// POST
// Add a skuavailability information
    const addskuavailability = function(req, res) {
        let skuavailability = new SkuAvailability(req.body);
        skuavailability.save(apiResponse.getStandardDBResponseHandler(res, true));
    };

// PUT
// Edit new skuavailability information
    const editskuavailability = function(req, res) {
        var id = req.params.id;
        SkuAvailability.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
    };


// DELETE
// Delete a skuavailability information
    const deleteskuavailability = function(req, res) {
        var id = req.params.id;
        return SkuAvailability.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));

};

// GET /api/skuavailability
// To get all skuavailability details
router.get('/getcolorcounter',  getColorCounter);
router.get('/getdaysincolorcount',getDaysInColorCount)

// POST /api/skuavailability
// To add a skuavailability
router.post('/skuavailability', addskuavailability);

// GET,PUT And DELETE /api/skuavailability/:id
// To get,update and delete skuavailability using _id
router.route('/skuavailability/:id')
    .get(getskuavailability)
    .put(editskuavailability)
    .delete(deleteskuavailability);



module.exports = router;