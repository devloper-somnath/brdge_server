// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    Department = mongoose.models.Department,
    ///User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

    // mongoose.set('debug', true);

// ALL
    const getallDepartments = function(req, res) {
        Department.find(apiResponse.getStandardDBResponseHandler(res, true));
    };

// GET 
// Retrive single departmnt information
    const getDepartment = function(req, res) {
        Department.findById(req.params.id,
        apiResponse.getStandardDBResponseHandler(res, true));
    };

// POST
// Add a department information
    const addDepartment = function(req, res) {
        let department = new Department(req.body);
        department.save(apiResponse.getStandardDBResponseHandler(res, true));
    };

// PUT
// Edit new department information
    const editDepartment = function(req, res) {
        var id = req.params.id;
        Department.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
    };


// DELETE
// Delete a deparment information
    const deleteDepartment = function(req, res) {
        var id = req.params.id;
        return Department.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));

};

// GET /api/department
// To get all deparmtent details
router.get('/departments', getallDepartments);

// POST /api/department
// To add a department
router.post('/department', addDepartment);

// GET,PUT And DELETE /api/department/:id
// To get,update and delete department using _id
router.route('/department/:id')
    .get(getDepartment)
    .put(editDepartment)
    .delete(deleteDepartment);



module.exports = router;