// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    Location = mongoose.models.Location,
    StockNorm = mongoose.models.StockNorm,
    NodeSKUMap = mongoose.models.NodeSKUMap,
    SkuComment = mongoose.models.SkuComment,
    SKUMaster = mongoose.models.SKUMaster,
    ///User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

    // mongoose.set('debug', true);



const getComments = function(req, res) {
  SkuComment.find(
    {"sku._id": ObjectId(req.params.id)},
    apiResponse.getStandardDBResponseHandler(res, true));
}

const postComment = function(req, res) {
    // console.log("node sku id is ",req.params.id)
    SKUMaster.findById(req.params.id, function(err, result) {
        
    let newComment = new SkuComment(req.body);
    newComment.sku = result;
    newComment.save(apiResponse.getStandardDBResponseHandler(res, false));
    if(result.comment_count) {
      result.comment_count++;
      
    } else {
      result.comment_count = 1;
    }
    result.save(function(){ console.log("count updated"); });
  });

}


// NodeSkuComment.findById(id,function(err,commentresult)  {
    // let nodeskuid = commentresult.nodesku._id;
      
    //   NodeSKUMap.update({"_id":nodeskuid},
    //   {$inc:{comment_count:-1}},function(err,sucess) {
    //     NodeSkuComment.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));
    //     })
    //   });

// DELETE
// Delete location information
  const deleteComment = function(req, res) {
    var id = req.params.id;
    //return NodeSkuComment.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));
    SkuComment.findById(id).lean().exec(function(err,commentresult)  {
      //console.log("comment result",commentresult)
    let skuid = commentresult.sku._id;
    //console.log("id is ",nodeskuid.toString())
      
        SKUMaster.update({"_id":skuid},
      {$inc:{comment_count:-1}},function(err,sucess) {
        SkuComment.findByIdAndRemove(id, apiResponse.getStandardDBResponseHandler(res, false));
        })
      });
  }


  router.get('/comments/sku/:id', getComments)
  router.post('/comments/sku/:id', postComment);
  router.delete('/comments/sku/:id', deleteComment);



module.exports = router;
