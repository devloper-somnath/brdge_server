// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    Node = mongoose.models.Node,
    DailyNodeLocation = mongoose.models.DailyNodeLocation,
    WeeklyNodeLocation = mongoose.models.WeeklyNodeLocation,
    MonthlyNodeLocation = mongoose.models.MonthlyNodeLocation,
    StockNorm = mongoose.models.StockNorm,
    NodeSKUMap = mongoose.models.NodeSKUMap,
    NodeMap = mongoose.models.NodeMap,
    moment = require('moment'),
    async = require("async"),
    multer = require('multer'),
    excelToJson = require('convert-excel-to-json'),

    ///User = mongoose.models.User,
    // ...
    auth = require('../utils/apiAuth'),
    _ = require("lodash"),

    apiResponse = require('../utils/apiResponse'),
    apiErrors = require('../utils/apiErrors'),
    config = require('../config/config');

    // mongoose.set('debug', true);

// daily data rerive data for node which has parent=null from dailyNodelocationAggrigate 

    const dailyNodeLocationParentData = function(req,res) {
      let selection = {}
      console.log("parent id is",req.query.parentid);
      if( req.query.parentid!=null ) {
        // node.parent._id
        selection['node._id'] = ObjectId(req.query.parentid)
      }
      else {
        selection['node.name'] = "India"
       }
       DailyNodeLocation.find(selection,function(err,result) {
        if (result.length==0) {
          let selection1 = {}
          selection1['node.location._id'] = ObjectId(req.query.parentid)
          DailyNodeLocation.find(selection1,apiResponse.getStandardDBResponseHandler(res, true)).limit(5).sort({ date: 1 });
        }
        else {
            apiResponse.sendResponse(result,200,res)
          }
        }).limit(5).sort({ date: 1 });
      
      // {"node.name":"India"}  
    //DailyNodeLocation.find({"node.name":"India"},apiResponse.getStandardDBResponseHandler(res, true)).limit(5).sort({ date: 1 });
   //DailyNodeLocation.find({'node._id':ObjectId("5f4385bd0c38992c271a9da6")},apiResponse.getStandardDBResponseHandler(res, true))    
}


//weekly data retrive from weeklynodelocationaggrigate for line chart
  const weeklyNodeLocationParentData = function(req,res) {
    console.log("parent id is",req.query.parentid);
    let selection ={}
    if( req.query.parentid!=null ) {
        //node.parent._id
        selection['node._id'] = ObjectId(req.query.parentid)
    }
    else {
        selection['node.name'] ="India"
      }
      WeeklyNodeLocation.find(selection,function(err,result) {
        if (result.length==0) {
          let selection1 = {}
          selection1['node.location._id'] = ObjectId(req.query.parentid)
          WeeklyNodeLocation.find(selection1,apiResponse.getStandardDBResponseHandler(res, true)).limit(5).sort({ date: 1 });
        }
        else {
         apiResponse.sendResponse(result,200,res)
        }
    }).limit(5).sort({ date: 1 });
   
  //WeeklyNodeLocation.find(selection,apiResponse.getStandardDBResponseHandler(res, true)).limit(5).sort({ date: 1 });
        
}

//weekly data retrive from weeklynodelocationaggrigate for line chart
    const monthlyNodeLocationParentData = function(req,res) {
    console.log("parent id is",req.query.parentid);
    let selection = {}
    if( req.query.parentid!=null ) {
        // node.parent._id
        selection['node._id'] =ObjectId(req.query.parentid)
    }
    else {
        selection['node.name'] ="India"
       }
       MonthlyNodeLocation.find(selection,function(err,result){
        if (result.length==0){
            let selection1 = {}
            selection1['node.location._id'] = ObjectId(req.query.parentid)
            MonthlyNodeLocation.find(selection1,apiResponse.getStandardDBResponseHandler(res, true)).limit(5).sort({ date: 1 });
        }
        else {
         apiResponse.sendResponse(result,200,res)
        }
    }).limit(5).sort({ date: 1 });
       
    //MonthlyNodeLocation.find(selection,apiResponse.getStandardDBResponseHandler(res, true)).limit(5).sort({ date: 1 });
        
}



 

// ALL dialy record
const getallDailyNodeLocation = function(req, res) {
    DailyNodeLocation.find(apiResponse.getStandardDBResponseHandler(res, true));
};

// all weekly record
const getallWeeklyNodeLocation = function(req, res) {
    WeeklyNodeLocation.find(apiResponse.getStandardDBResponseHandler(res, true));
};
// all monthly record
const getallMonthlyNodeLocation = function(req, res) {
    MonthlyNodeLocation.find(apiResponse.getStandardDBResponseHandler(res, true));
};



// POST
// Add a  new record to dailynodeaggrigate table
const addDailyNodeLocationAggrigate = function(req, res) {
    let dailyNodeLocation = new DailyNodeLocation(req.body);
    dailyNodeLocation.save(apiResponse.getStandardDBResponseHandler(res, true));
};

// Add a  new record to weeklynodeaggrigate table
const addWeeklyNodeLocationAggrigate = function(req, res) {
    let weeklyNodeLocation = new WeeklyNodeLocation(req.body);
    weeklyNodeLocation.save(apiResponse.getStandardDBResponseHandler(res, true));
};

// Add a  new record to monthlynodeaggrigate table
const addMonthlyNodeLocationAggrigate = function(req, res) {
    let monthlyNodeLocation = new MonthlyNodeLocation(req.body);
    monthlyNodeLocation.save(apiResponse.getStandardDBResponseHandler(res, true));
};

// PUT
// Edit
const editDailyNodeLocation = function(req, res) {
    var id = req.params.id;
    DailyNodeLocation.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
};
//put 
// edit weekly
const editWeeklyNodeLocation = function(req, res) {
    var id = req.params.id;
    WeeklyNodeLocation.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
};

//put 
// edit weekly
const editMonthlyNodeLocation = function(req, res) {
    var id = req.params.id;
    MonthlyNodeLocation.findByIdAndUpdate(id, { $set: req.body },
        apiResponse.getStandardDBResponseHandler(res, false));
};



// Private function
const updateNodes = function() {
  Node.find({}, function(err, nodes) {
    async.each(nodes, function(node, callback) {

      async.parallel([
        function(innerCallback) {
          DailyNodeLocation.update(
            {"node._id": node._id},
            {$set: {node: node}},
            {multi: true},
            innerCallback
          );
        },
        function(innerCallback) {
          StockNorm.update(
            {"consumer._id": node._id},
            {$set: {consumer: node}},
            {multi: true},
            innerCallback
          );
        },
        function(innerCallback) {
          StockNorm.update(
            {"supplier._id": node._id},
            {$set: {supplier: node}},
            {multi: true},
            innerCallback
          );
        },
        function(innerCallback) {
          NodeSKUMap.update(
            {"node._id": node._id},
            {$set: {node: node}},
            {multi: true},
            innerCallback
          );
        },
        function(innerCallback) {
          NodeMap.update(
            {"child._id": node._id},
            {$set: {child: node}},
            {multi: true},
            innerCallback
          );
        },
        function(innerCallback) {
          NodeMap.update(
            {"supplier._id": node._id},
            {$set: {supplier: node}},
            {multi: true},
            innerCallback
          );
        }
      ], function(err, result) {
        callback();
      })
    }, function(err) {
      console.log("\n\nDone updating");
    });
  });
}

// updateNodes();

// GET /api/department
// To get all 
router.get('/dailynodelocationaggrigate', getallDailyNodeLocation);
router.get('/weeklynodelocationaggrigate', getallWeeklyNodeLocation);
router.get('/monthlynodelocationaggrigate', getallMonthlyNodeLocation);


// POST /api/dailynodelocation,weeklynodelocation,monthlynodelocation
// To add a 
router.post('/dailynodelocationaggrigate', addDailyNodeLocationAggrigate);
router.post('/weeklynodelocationaggrigate', addWeeklyNodeLocationAggrigate);
router.post('/monthlynodelocationaggrigate', addMonthlyNodeLocationAggrigate);


// retrive daily data which parent is null

router.get('/dailynodelocationparentdata',dailyNodeLocationParentData);
router.get('/weeklynodelocationparentdata',weeklyNodeLocationParentData);
router.get('/monthlynodelocationparentdata',monthlyNodeLocationParentData)

// GET,PUT And DELETE /api/dailynodeocation/:id
// To get,update and delete a  dailynodelocation by using _id
router.route('/dailynodelocation/:id')
    
    .put(editDailyNodeLocation)
    

    //edit
    router.put("/weeklynodelocation/:id",editWeeklyNodeLocation)

    router.put("/monthlynodelocation/:id",editMonthlyNodeLocation)




module.exports = router;